
Vue.use(VueTables.ClientTable);
var user_competitions = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
      //podatki za modal
      imeTekmovanja: "",
      checkedNames: [],
      columns: ['Izbira','Naziv'],
      tableData: [
      ],
      options: {
        headings: {
          Naziv: 'Naziv šole',
          Izbira: 'Prijava/odjava'
        },
        texts: {
          filterPlaceholder:"Iskanje po nazivu tekmovanja...",
        },
      },

      sortable: ['Naziv'],
      filterable: ['Naziv'],
      selectedCompetitionId: -1,
      selectedCompetitionName: "",

      //tekmovanja za zavihek moja tekmovanja
      competitionsAndSchools: [],

      //tekmovanja za zavihek vsa tekmovanja
      allCompetitionData: [],

      //tekmovanja za zavihek skrita tekmovanja
      hiddenCompetitions: [],

      //modal
      competition_info: "",
      nicerLoginDate: "",
      nicerCompetitionDate: "",
      isLoginEnabled: true,
      currDate:"",
      loginDate:"",
      obdobje:"",
      modal_obdobje:"",
      nicerCompetitionDate_from:"",
      nicerCompetitionDate_to:"",
  },
  computed: {
    //lepši izpis datumov
    allCompetitionDataList () {
      let newArr = [...this.allCompetitionData]
      newArr.map(el => {
        if (el.obdobje === 0) {
          let newDate = el.competition_date.split("-");
          if(newDate.length >= 3) {
            return el.competition_date = newDate[2]+"."+newDate[1]+"."+newDate[0];
          }
        }
        if (el.obdobje ===1) {
          let newDate = el.competition_date_from.split("-");
          if(newDate.length >= 3) {
            el.competition_date_from = newDate[2]+"."+newDate[1]+"."+newDate[0];
          }
          newDate = el.competition_date_to.split("-");
          if(newDate.length >= 3) {
            el.competition_date_to = newDate[2]+"."+newDate[1]+"."+newDate[0];
          }
          return el
        }
      })
      newArr.map(el => {
        let newDate = el.start_login_date.split("-");
        if(newDate.length >= 3) {
          el.start_login_date = newDate[2]+"."+newDate[1]+"."+newDate[0];
        }
        return el;
      })
      return newArr
    }
  },
  methods: {
    //trenutno izbrano tekmovanje za modal
    selectedCompetition: function(competitionId,competitionName) {
      this.selectedCompetitionId = competitionId;
      this.selectedCompetitionName = competitionName;
      this.showCompetitionModal(competitionId);
      this.getCompetitionInfo(competitionId);
    },

    //pridobi vsa tekmovanja in šole preko katerih je uporabnik prijavljen, zavihek moja tekmovanja
    getUserCompetitions: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      axios
      .get("/api/user/user_competitions/",false,{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          sez_sol = [];
          id_tekmovanj = [];
          
          //struktura podatkov, ki jo dobimo iz be: [{ID_Sola: 1, ID_Tekmovanje: 43, Ime_sola: "sola1", Naziv: "naziv1"},
          //{ID_Sola: 2, ID_Tekmovanje: 43, Ime_sola: "sola2", Naziv: "naziv1"} ....]

          //struktura podatkov, ki si jo želimo: [{ID_Tekmovanje:43, Naziv: naziv1, sez_sol:[sola1,sola2]}]

          //pojdi čez cel response
          response.data.forEach(i => {
            //če ima i-to tekmovanje že seznam šol ne naredi ničesar, sicer pojdi še enkrat čez cel response in dodaj šole pod i-to tekmovanje (željena struktura)
            //ter i-to tekmovanje shrani
            if(!id_tekmovanj.includes(i.ID_Tekmovanje)) {
              //pridobi vse šole za i-to tekmovanje in dodaj i-to tekmovanje v seznam tekmovanj, ki so obdelana (imajo seznam šol)
              response.data.forEach(j => {
                if(j.ID_Tekmovanje === i.ID_Tekmovanje) {
                  if(sez_sol.length == 0) {
                    id_tekmovanj.push(i.ID_Tekmovanje);
                  }
                  sez_sol.push(j.Ime_sola);
                }
              });
              //shrani i-to tekmovanje
              this.competitionsAndSchools.push({
                ID_Tekmovanje:i.ID_Tekmovanje,
                Naziv:i.Naziv,
                sez_sol: sez_sol
              });

              //resetiraj seznam šol za naslednje i-to tekmovanje
              sez_sol = [];
            }
          });
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    //klic funkcije za spremembo checkboxa (odjava ali prijava na tekmovanje)
    checkboxChange(schoolId,schoolName) {
      //odloči se ali je uporabnik naredil check ali unchek
      let isCheck = false;
      if(this.checkedNames.includes(schoolId)) {
        isCheck = true;
      }

      this.competition_login(this.selectedCompetitionId,schoolId,isCheck,schoolName)
    },

    //prijava ali odjava na tekmovanje v okviru šole
    competition_login: function(competitionId,schoolId,isCheck,schoolName) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      axios
      .post("/api/user/competition_login/",{competitionId:competitionId,schoolId:schoolId,isCheck:isCheck}, {headers: headers})
      .then((response) => {
        if(response.status == 200) {
          let tekmovanjeObstaja = false;
          //uporabnik se je prijavil na tekmovanje v sklopu šole
          if(isCheck) {
            
            this.competitionsAndSchools.forEach(el => {
              //pridobi tekmovanje
              //če tekmovanje obstaja pomeni, da je uporabnik prijavljen na njega z vsaj 1 šolo
              //to pomeni, da je potrebno dodati v seznam vseh šol
              if (el.ID_Tekmovanje === competitionId) {
                  if(!el.sez_sol.includes(schoolName)) {
                    el.sez_sol.push(schoolName)
                  }

                  //robni pogoj, šola je lahko že od začetka v checked names
                  //ker je uporabnik prijavaljen na njej
                  if(!this.checkedNames.includes(schoolId)) {
                    this.checkedNames.push(schoolId);
                  }
                  tekmovanjeObstaja = true;
              }
            })
            //TEKMOVANJE NE OBSTAJA TO POMENI DA SE UPORABNIK PRVIČ PRIJAVLJA NA TEKMOVANJE V SKLOPU ŠOLE
            if(tekmovanjeObstaja === false) {

                this.competitionsAndSchools.push({
                  ID_Tekmovanje:this.selectedCompetitionId,
                  Naziv:this.selectedCompetitionName,
                  sez_sol: [schoolName],
                });
                if(!this.checkedNames.includes(schoolId)) {
                  this.checkedNames.push(schoolId);
                }
            }
          }
          //uporabnik se je odjavil iz tekmovanja v sklopu šole
          else {
            this.competitionsAndSchools.forEach(el => {
              if (el.ID_Tekmovanje === competitionId) {
                //izbriši iz uporabnikovega seznama šol
                el.sez_sol = el.sez_sol.filter(t =>  t !== schoolName);
                this.checkedNames = this.checkedNames.filter(t => t !== schoolId)

                //preveri ali ni več prijavaljen na tekmovanje preko nobene šole
                if(el.sez_sol.length === 0) {
                  this.competitionsAndSchools = this.competitionsAndSchools.filter(t => t.ID_Tekmovanje !== el.ID_Tekmovanje);
                }
              }
            })
          }

          //toast o uspešnosti akcije za uporabnika
          let title = "";
          let message = "";
          if(isCheck) {
            title="Prijava uspešna";
            message = "Prijava na tekmovanje preko šole uspešna";
          } else {
            title="Odjava uspešna";
            message="Odjava na tekmovanje preko šole uspešna";
          }
          this.$bvToast.toast(message, {
            title: title,
            autoHideDelay: 3000,
            appendToast: true,
            variant: 'success',
            solid: true
          });
        }
      })
      .catch(error => {
        if(error.response.status == 400 && error.response.data == "datum je manjši od datuma za začetek prijave") {
          this.$bvToast.toast("Datum začetka prijave večji", {
            title: "Neuspešno",
            autoHideDelay: 3000,
            appendToast: true,
            variant: 'error',
            solid: true
          });
        }
        if(error.response.data != "datum je manjši od datuma za začetek prijave") {
          this.$bvToast.toast("Prijava nesupešna. Kontaktirajte administratorja.", {
            title: "Neuspešno",
            autoHideDelay: 3000,
            appendToast: true,
            variant: 'error',
            solid: true
          });
        }
        
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    //pridobi vse podatke za modal
    showCompetitionModal(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      //pridobi podatke o uporabnikovih morebitnih prijavah
      axios
      .get("/api/user/user_competition_subscripitons/", {params: { competitionId:competitionId+""}},{headers: headers})
      .then((response) => {
        if(response.status == 200) {
          this.checkedNames = [];
          this.imeTekmovanja = response.data.ime_Tekmovanja;

          //pridobi podatke za v tabelo. Šole na katerih uporabnik ni potrjen izpusti
          this.tableData=response.data.user_schools.filter(el => el.Potrjen == 1);

          //pridobi vse šole s katerimi je uporabnik prijavljen na tekmovanje
          for(let i = 0; i < response.data.user_schools.length; i++) {
              let sola = response.data.user_schools[i];
              if(sola.Prijavljen == true) {
                  this.checkedNames.push(sola.ID_Sola);
              }
          }
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    getCompetitionInfo(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      //pridobi podatke o uporabnikovih morebitnih prijavah
      axios
      .get("/api/user/competition_info/", {params: { competitionId:competitionId+""}},{headers: headers})
      .then((response) => {
        if(response.status == 200) {
            this.competition_info = response.data;
            let date = "";
            date = response.data.start_login_date.split("-");
            this.nicerLoginDate = date[2]+"."+date[1]+"."+date[0];
            this.modal_obdobje = response.data.obdobje;
            
            if (this.modal_obdobje === 0) {
              date = response.data.competition_date.split("-");
              this.nicerCompetitionDate = date[2]+"."+date[1]+"."+date[0];
            }

            if (this.modal_obdobje === 1) {
              date = response.data.competition_date_from.split("-");
              this.nicerCompetitionDate_from = date[2]+"."+date[1]+"."+date[0];  
              date = response.data.competition_date_to.split("-");
              this.nicerCompetitionDate_to = date[2]+"."+date[1]+"."+date[0];   
            }

            //preveri ali se lahko uporabnik prijavi na tekmovanje
            let tmp = new Date().toISOString().slice(0, 10);
            let start_login_date = new Date(response.data.start_login_date).getTime();
            let today = new Date(tmp).getTime();
            this.currDate = tmp;
            this.loginDate = response.data.start_login_date;

            if(today >= start_login_date) {
              this.isLoginEnabled = true;
            } else {
              this.isLoginEnabled = false;
            }
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },
    
    getAllCompetitions: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/user/sodeluje_tekmovanje/", false,{headers: headers})
      .then((response) => {
        if(response.status == 200) {
          this.allCompetitionData=response.data.filter(x => x.hidden === "0");
          this.hiddenCompetitions=response.data.filter(x => x.hidden !== "0");
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },
    hideCompetition(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      data = {
        competitionId: competitionId
      }
      if (confirm('Ste prepričani, da želite skriti tekmovanje? V primeru, da ste na tekmovanje prijavljeni v sklopu šol, potem ne boste več.')) {
        axios
        .post("/api/user/hide_competition/", data,{headers: headers})
        .then((response) => {
          if(response.status == 201) {
            //add competition to hidden compettions
            this.hiddenCompetitions.push(this.allCompetitionData.filter(x => x.ID_Tekmovanje === competitionId)[0]);

            //delete from visible competitions
            this.allCompetitionData = this.allCompetitionData.filter(x => x.ID_Tekmovanje !== competitionId);

            //delete from my competitions
            this.competitionsAndSchools = this.competitionsAndSchools.filter(x => x.ID_Tekmovanje !== competitionId);
          }
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      }
    },
    unHideCompetition(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      data = {
        competitionId: competitionId
      }
      if (confirm('Ste prepričani, da želite odkriti tekmovanje?')) {
        axios
        .post("/api/user/un_hide_competition/", data,{headers: headers})
        .then((response) => {
          if(response.status == 201) {  
            //add competition to all competitions
            this.allCompetitionData.push(this.hiddenCompetitions.filter(x => x.ID_Tekmovanje === competitionId)[0]);
            
            //delete competition from hidden competitions
            this.hiddenCompetitions = this.hiddenCompetitions.filter(x => x.ID_Tekmovanje !== competitionId);
          }
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      }
    },
  },
});
user_competitions.getUserCompetitions();
user_competitions.getAllCompetitions();
