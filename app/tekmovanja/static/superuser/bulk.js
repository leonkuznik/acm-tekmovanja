var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        status:[],
        textareaValue: "",
        parsedCsvData: [],
        validData: [],
    },
    methods: {
        parseToCsv() {
            this.status=[];
            if(this.textareaValue === "") {
                this.status.push("Vnesti morate vsaj enega uporabnika");
                return;
            }

            let csvObject = Papa.parse(this.textareaValue);
            if(this.validateCsv(csvObject.errors) && this.validateInput(csvObject.data)) {
                this.bulkInsert();
            }
        },
        validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        validateCsv(errors) {
            this.status.push("Preverjanje CSV");
            if(errors.length > 0) {
                errors.map(er => this.status.push(er.message))
                return false;
            }
            return true;
        },
        validateInput(data) {
            this.status.push("Preverjanje vhodnih podatkov");
            this.validData=[];
            line=0;
            valid=true;
            data.forEach(arr => {
                line++;
                if (arr.length == 4) {
                    first_name=arr[0];
                    last_name=arr[1];
                    username=arr[2];
                    email=arr[3];

                    if(username.includes("@")) {
                        this.status.push("Napaka. Vrstica "+line+" uporabinško ime ne sme vsebovati znaka @");
                        this.validData=[];
                        valid=false;
                    }
                    if(this.validateEmail(email)==false) {
                        this.status.push("Napaka. Vrstica "+line+" nima veljavnega email naslova");
                        this.validData=[];
                        valid=false;
                    }
                    if(valid) {
                        this.validData.push(arr);
                    }
                } else{
                    if(arr[0] != "") {
                        this.status.push("Napaka. Vrstica "+line+" vsebuje "+arr.length+" atributov");
                        this.validData=[];
                        valid=false;
                    }
                }
            });
            return valid;
        },
        bulkInsert: function() {
            this.status.push("Pošiljanje...");
                var headers = {
                    'X-CSRFToken': getCookie("csrftoken"),
                  }
                var data = {
                    users: this.validData
                };
                  axios
                  .post("/api/superuser/bulk_users/", data, {headers:headers})
                  .then((response) => {
                    if(response) {
                        this.status.push("Uporabniki uspešno vnešeni");
                    }
                  })
                  .catch(error => {
                      if(error.response.status == 409) {
                          this.status.push("Uporabnik z up. imenom ali mailom že obstaja: "+error.response.data);
                          return;
                      }
                    switch(error.response.data) {
                        case 'no_required_parameters' : this.status.push("Napaka pri procesiranju: manjkajoči parametri"); break;
                        case 'no_data' : this.status.push("Napaka pri procesiranju: prazen seznam uporabnikov (vnesite vsaj 1 uporabnika)"); break;
                        case 'up_ime_vsebuje_nedovoljen_znak': this.status.push("Napaka pri procesiranju: Up ime vsebuje nedovoljen znak(@)"); break;
                        case 'podvojeni_podatki': this.status.push("Napaka pri procesiranju: Podvojeni podatki (up ime ali email naslov)."); break;
                        case 'data_length_conflict': this.status.push("Napaka pri procesiranju: V neki vrstici je preveč ali premalo parametrov."); break;
                        case 'napacen_email': this.status.push("Napaka pri procesiranju: Vnešena napačna struktura email naslova."); break;
                        default: this.status.push("Napaka pri procesiranju na zalednem delu")
                    }
                  });
            },
        }
    })