var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        status:"",
        masterMail:"",
        list: [{
            id: 1,
            value: ""
        }
      ],
    },
    computed: {
      listByBreaks() {
        return this.list.map(e => {
          return e.value.split("\n");
        });
      }
    },
    methods: {
        validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        mergeInsert: function() {
            this.status="Preverjanje vhodnih podatkov";
            if(this.validate()) {
                var headers = {
                    'X-CSRFToken': getCookie("csrftoken"),
                  }
                var data = {
                    emails: this.listByBreaks[0],
                    masterEmail: this.masterMail
                };

                if(this.listByBreaks.length < 1) {
                    this.status="Vnesti morate vsaj 2 uporabnika";
                    return;
                }

                this.status="Pošiljanje...";
                  axios
                  .post("/api/superuser/merge_users/", data, {headers:headers})
                  .then((response) => {
                    if(response) {
                        this.status = "Merganje uspešno. Vsi maili so bili razposlani. V primeru, da uporabnik se ne bo potrdil preko mailo znotraj časovnega okvirja merganje ne bo več uspešno.";
                    }
                  })
                  .catch(error => {
                      if(error.response.status == 409) {
                          this.status = "Zapis z mailom že obstaja: "+error.response.data+". V kolikor se uporabnik ne bo potrdil, bo email ponovno na voljo (čez največ 6 ur).";
                          return;
                      }
                    switch(error.response.data) {
                        case 'email_not_found' : this.status = "Napaka pri procesiranju: eden izmed mailov ne obstaja"; break;
                        case 'napacen_email' : this.status = "Napaka pri procesiranju: eden izmed mailov je napačen"; break;
                        case 'user_not_confirmed' : this.status = "Napaka pri procesiranju: eden izmed mailov še ni aktiven (atribut is_active pri user modelu). Prosimo potrdite ga ročno ali prosite uporabnika naj ga potrdi."; break;
                        case 'only_users' : this.status = "Napaka pri procesiranju: Združujete lahko le navadne uporabnike."; break;
                      }
                  });
                }
            },
        validate: function() {
            line=0;
            valid=true;

            if(this.validateEmail(this.masterMail) == false) {
                this.status="Master mail ni veljaven email naslov";
                valid = false;
            }

            if(!this.listByBreaks[0].includes(this.masterMail)) {
                this.status="Master mail mora biti eden izmed vnešenih mailov";
                valid = false;
            }

            this.listByBreaks[0].forEach(email => {
                console.log(email)
                line++;
                if(this.validateEmail(email)==false && email != "") {
                    this.status="Napaka. Vrstica "+line+" nima veljavnega email naslova";
                    valid=false;

                }
            });

            return valid;
        }
        }
    })