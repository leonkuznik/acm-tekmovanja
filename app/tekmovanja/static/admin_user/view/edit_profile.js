var emailRegExp = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      ime: "",
      priimek: "",
      up_ime: "",
      email: "",
      status: "",
      geslo_status: "",
      geslo_status2: "",
      geslo1: "",
      geslo2: "",
      geslo3: "",
      user: "",
      usEmail: "",
      serverName: "",
      portNumber: "",
      izbris_status: ""
    },
    methods: {
      onInit() {
        if(localStorage.getItem('user')) {
          this.user = JSON.parse(localStorage.getItem('user'));
        } else {
          this.get_user_info();
        }
        this.ime = this.user.first_name;
        this.priimek = this.user.last_name;
        this.up_ime = this.user.username;
        this.email = this.user.email;
      },

      get_user_info: function() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/user/get_user_info/", false, {headers:headers})
        .then((response) => {
          if(response) {
              localStorage.setItem('user', JSON.stringify(response.data.user));
              this.user = JSON.parse(localStorage.getItem('user'));
            }
        })
        .catch(error => {
          alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
        });
      },

      change_data() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'Ime': this.ime,
          'Priimek': this.priimek,
          'UpIme': this.up_ime,
          //'email': this.email,
        };

        if(this.ime == this.user.first_name && this.priimek == this.user.last_name && this.up_ime == this.user.username && this.email == this.user.email) {
          this.status="Spremenili niste nobenega podatka.";
          return;
        }
        if (confirm('Ste prepričani, da želite spremeniti podatke?')) {
          this.status = "Pošiljanje ..."
          axios
          .post("/api/upravitelj_tekmovanj/admin_change_profile/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) { 
              this.status = "Podatki so uspešno spremenjeni, prosimo prijavite se ponovno.";
            }
          })
          .catch(error => {
            if(error.response.status == 407) {
              this.status = "Uporabniško ime ne sme vsebovati znaka @";
            }
            if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
            if(error.response.status === 409) {
                switch(error.response.data) {
                    case 'mankajoci_podatki': this.status = "Manjkajoči podatki"; break;
                    case 'predolgo_up_ime_ali_email': this.status = "Uporabniško ime ali email sta predolga"; break;
                    case 'up_ime_zasedeno': this.status = "Uporabniško ime zasedeno"; break;
                    case 'email_zaseden': this.status = "Email naslov je že zaseden"; break;
                }
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
          });
        }
      },

      change_password() {
        if (this.geslo1 !== this.geslo2) {
          this.geslo_status = "Gesli se ne ujemata";
          return;
        }
        if (this.geslo1.length < 5) {
          this.geslo_status = "Geslo mora biti dolgo vsaj 5 znakov";
          return;
        }
        
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'geslo': this.geslo1,
        };

        if (confirm('Ste prepričani, da želite spremeniti geslo?')) {
          this.geslo_status = "Pošiljanje ..."
          axios
          .post("/api/upravitelj_tekmovanj/admin_change_password/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) {      
              this.geslo_status = "Uspešna sprememba gesla. Ob ponovni prijavi boste morali vnesti novo geslo.";
            }
          })
          .catch(error => {
            if(error.response.status === 409) {
              this.geslo_status = "Napaka.";
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        }
      },

      delete_user() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        if (confirm('Ste prepričani, da se želite izbrisati?')) {
          axios
          .delete("/api/upravitelj_tekmovanj/delete_admin_user/",{headers: headers})
          .then((response) => {
            if(response.data === "zadnji_admin") {
              this.izbris_status = "Ker ste zadnji upravitelj tekmovanj v sistemu, se iz sistema ne morete izbrisati";
            } else {
              window.location.replace("/login");
            }  
          })
          .catch(error => {
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        }
      },

      setSettings: function(){
        //this.usEmail this.geslo3

        if(this.usEmail == "" && this.geslo3 == ""){
          this.geslo_status2 = "Vnesti morate obe polji!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }else if(this.usEmail == ""){
          this.geslo_status2 = "Vnesti morate e-pošto!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }else if(this.geslo3 == ""){
          this.geslo_status2 = "Vnesti morate geslo!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }else{
          // Validacija e-pošte!
          // Sintaksa e-pošte
          if(emailRegExp.test(this.usEmail)){
              console.log("Valid");
              var headers = {
                  'X-CSRFToken': getCookie("csrftoken"),
              }
              axios
              .post("/api/upravitelj_tekmovanj/admin_change_settings/", {eposta: this.usEmail, gesl: this.geslo3}, {headers: headers})
              .then((response) => {
                if(response.status === 201) {
                  this.geslo_status2 = "Uspešno ste spremenili nastavitve";
                  document.getElementsByClassName("text-info")[2].style.setProperty('color', 'green', 'important');
                  document.getElementsByClassName("text-info")[2].style.textAlign = "center";
                }
              })
              .catch(error => {
                if(error.response.status === 409) {
                  this.geslo_status = "Napaka.";
                }
                if(error.response.status == 500) {
                  console.log("Something went wrong");
                }
              });
          }else{
            this.geslo_status2 = "Vnesli ste nepravilno obliko e-pošte!";
            document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
            document.getElementsByClassName("text-info")[2].style.textAlign = "center";
          }
        }
      },

      saveSettings: function(){
        //this.usEmail this.geslo3
        if(this.serverName == "" && this.portNumber == "" && this.usEmail == ""){
          this.geslo_status2 = "Vnesti morate vsa polja!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }else if(this.serverName == ""){
          this.geslo_status2 = "Vnesti je potrebno naslov e-poštnega strežnika!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }
        else if(this.portNumber == ""){
          this.geslo_status2 = "Vnesti je potrebno številko vrat!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }
        else if(this.usEmail == ""){
          this.geslo_status2 = "Vnesti je potrebno e-poštni naslov!";
          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
        }else{
          // Validacija e-pošte!
          // Sintaksa e-pošte
          if(emailRegExp.test(this.usEmail)){
              console.log("Valid");
              var headers = {
                  'X-CSRFToken': getCookie("csrftoken"),
              }
              // pridobi admina
              if(localStorage.getItem('user')) {
                this.user = JSON.parse(localStorage.getItem('user'));
              } else {
                this.get_user_info();
              }
              admin_id = this.user.id;
              axios
              .post("/api/upravitelj_tekmovanj/admin_save_settings/", {serName:this.serverName,portN:this.portNumber,email:this.usEmail,userCreate:admin_id}, {headers: headers})
              .then((response) => {
                if(response.status === 201) {
                  this.geslo_status2 = "Uspešno ste shranili nastavitve";
                  document.getElementsByClassName("text-info")[2].style.setProperty('color', 'green', 'important');
                  document.getElementsByClassName("text-info")[2].style.textAlign = "center";
                }
              })
              .catch(error => {
                if(error.response.status === 409) {
                  this.geslo_status = "Napaka.";
                }
                if(error.response.status === 405) {
                  this.geslo_status2 = "Vnos s tem uporabniškim imenom že obstaja!";
                  document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
                  document.getElementsByClassName("text-info")[2].style.textAlign = "center";
                }
                if(error.response.status == 500) {
                  console.log("Something went wrong");
                }
              });
          }else{
            this.geslo_status2 = "Vnesli ste nepravilno obliko e-pošte!";
            document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
            document.getElementsByClassName("text-info")[2].style.textAlign = "center";
          }
        }
      }
    }
  });

  app.onInit();
