// Regular expression from W3C HTML5.2 input specification:
// https://www.w3.org/TR/html/sec-forms.html#email-state-typeemail
var emailRegExp = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

var app = new Vue({
  // root node
  el: "#app",
  // the instance state
  data() {
      return{
        submitted: false,
        tag: '',
        tags: [],
        autocompleteItems: [],
        validation: [{
          classes: 'email',
          rule: emailRegExp,
        }],
        groups: [],
        users:[]
      };
  },
  methods: {
    // submit form handler
    submit: function() {
      if(this.tags.length == 0){
        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // open the modal
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
      }else{
        console.log("SUBMIT!!!");
        // POŠLJEMO MAIL!
        // pošljemo samo valid vnose!
        // this.tags = this.tags.filter(person => person.tiClasses.length == 1);
        // var rec = this.tags.map(a=>a.text);
        // var sub = document.getElementById("subject").value;
        // var mesHtml = document.getElementById("editor").children[0].innerHTML;
        // var mes = document.getElementById("editor").children[0].outerText;

        // TODO: Sestavi files po uporabnikovih zahtevah!

        //console.log(ee.files);
        //console.log(sav);
        if(sav.length == 0){
          // POŠLJEMO MAIL!
          // pošljemo samo valid vnose!
          this.tags = this.tags.filter(person => person.tiClasses.length == 1);
          var rec = this.tags.map(a=>a.text);
          var sub = document.getElementById("subject").value;
          var mesHtml = document.getElementById("editor").children[0].innerHTML;
          var mes = document.getElementById("editor").children[0].outerText;

          app.propPassword();
          //app.sendMessage(rec,sub,mes,mesHtml);
        }else{
          app.saveFile(sav);
        }
      }
    },

    addTagRemove: function(){
        document.getElementsByClassName('ti-new-tag-input')[0].placeholder = "";
    },

    getMentors: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/list_users/",false,{ headers: headers } )
      .then((response) => {
        if(response.status == 200) {
          // Napolni e-maile od posameznih mentorjev
          for (var i = 0; i < response.data.length; i++) {
            this.autocompleteItems.push({text: response.data[i].email});
          }
          this.users = response.data;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    getGroups: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
       // pridobi admina
      if(localStorage.getItem('user')) {
        this.user = JSON.parse(localStorage.getItem('user'));
      } else {
        this.get_user_info();
      }
      admin_id = this.user.id;
      axios
      .get("/api/upravitelj_tekmovanj/get_groups/",{params:{idA:admin_id}},{ headers: headers } )
      .then((response) => {
        if(response.status == 200) {
          // Napolni skupine
          for (var i = 0; i < response.data.length; i++) {
            this.autocompleteItems.push({text: response.data[i].name});
          }
          this.groups = response.data;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    get_user_info: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/get_user_info/", false, {headers:headers})
      .then((response) => {
        if(response) {
            localStorage.setItem('user', JSON.stringify(response.data.user));
            this.user = JSON.parse(localStorage.getItem('user'));
          }
      })
      .catch(error => {
        alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
      });
    },

    checkIfGroup: function(selectedName){
      // console.log(selectedName);
      // console.log(this.groups);
      if (this.groups.filter(item=> item.name == selectedName).length != 0){
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/upravitelj_tekmovanj/get_users_from_groups/",{params: {group_name:selectedName}}, {headers: headers})
        .then((response) => {
          if(response.status == 200) {
            groupTable = [];
            for(i = 0; i< response.data.length; i++){
              var usersFilter =  this.users.filter(function(guser) {
                return guser.id == response.data[i].user_id_id;
              });
              groupTable.push(usersFilter[0].email);
            }
            // console.log(groupTable);

            // odstrani skupino iz tags
            this.tags = this.tags.filter(person => person.text != selectedName);
            // dodaj v tags vse iz groupTable
            for (var i = 0; i < groupTable.length; i++) {
              // PREVERI DUPLIKATE
              if (this.tags.filter(item=> item.text == groupTable[i]).length == 0){
                // TODO Dodaj razred tiClasses!!!
                this.tags.push({text: groupTable[i],tiClasses: ["ti-valid"]});
                //this.tags.push({tiClasses: ["ti-valid"]});
              }
            }
            // console.log(this.tags);
          }
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      }
    },

    sendMessage: function(r,s,m,mhtml) {
      var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/upravitelj_tekmovanj/admin_send_message/", {recipients:r,subject:s+"",message:m+"", html:mhtml+""}, {headers: headers})
      .then((response) => {
        if(response.status == 200) {
          // Prikaži da je sporočilo uspešno poslano
          // Get the modal
        var modal = document.getElementById("myModal1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // open the modal
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        var el = document.getElementById('zapri');
        el.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 405) {
          // Prikaži da je sporočilo uspešno poslano
          // Get the modal
          var modal = document.getElementById("myModal3");

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName("close")[0];

          // open the modal
          modal.style.display = "block";

          // When the user clicks on <span> (x), close the modal
          var el = document.getElementById('zapri3');
          el.onclick = function() {
              modal.style.display = "none";
          }

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = "none";
              }
          }
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    fillTags: function(usrId){
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/get_user_by_id/",{params: { userId:usrId}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          this.tags.push({text: response.data["email"],tiClasses: ["ti-valid"]});
        }
      })
      .catch(error => {
        if(error.response.status == 403) {    
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    saveFile:function(files){
      const data = new FormData(); // creates a new FormData object
      if(files.length == 1){
        data.append('file', files[0], files[0].name);
      }else{
        for(var i = 0; i < files.length; i++){
          data.append('file', files[i], files[i].name); // add your file to form data
        }
      }

      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
        'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
      }
      axios
      .post("/api/upravitelj_tekmovanj/save_file/",data,{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          this.tags = this.tags.filter(person => person.tiClasses.length == 1);
          var rec = this.tags.map(a=>a.text);
          var sub = document.getElementById("subject").value;
          var mesHtml = document.getElementById("editor").children[0].innerHTML;
          var mes = document.getElementById("editor").children[0].outerText;
          app.propPassword();
          //app.sendMessage(rec,sub,mes,mesHtml);
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    fillFrom: function(){
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      // pridobi admina
      if(localStorage.getItem('user')) {
        this.user = JSON.parse(localStorage.getItem('user'));
      } else {
        this.get_user_info();
      }
      admin_id = this.user.id;
      axios
      .get("/api/upravitelj_tekmovanj/get_user_settings/",{params: {idU:admin_id}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          console.log(response.data);
          $.each(response.data, function (idx, obj) {
            $('#from').append($('<option>', {
              value: obj.ID_Setting,
              text : obj.posta
            }));
          });
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    propPassword:function(){
       // Prikaži da je sporočilo uspešno poslano
        // Get the modal
        var modal = document.getElementById("myModal2");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // open the modal
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        var el = document.getElementById('zapri2');
        el.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    },

    setSettings:function(){
      var geslo = document.getElementById("gesl").value;
      var modal = document.getElementById("myModal2");
      modal.style.display = "none";
      var mailId = $("#from").val();

      //Nastavi datoteko secrets in pošlji pošto!
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
       // pridobi admina
      if(localStorage.getItem('user')) {
        this.user = JSON.parse(localStorage.getItem('user'));
      } else {
        this.get_user_info();
      }
      admin_id = this.user.id;
      axios
      .post("/api/upravitelj_tekmovanj/set_settings/",{Id_a:admin_id,idmail:mailId,pass:geslo},{ headers: headers } )
      .then((response) => {
        if(response.status == 201) {
          // pošlji in nato izbriši geslo
          this.tags = this.tags.filter(person => person.tiClasses.length == 1);
          var rec = this.tags.map(a=>a.text);
          var sub = document.getElementById("subject").value;
          var mesHtml = document.getElementById("editor").children[0].innerHTML;
          var mes = document.getElementById("editor").children[0].outerText;
          app.sendMessage(rec,sub,mes,mesHtml);
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    }
  },
  watch: {
    // watching nested property
    "email.value": function(value) {
      this.validate("email", value);
    }
  },

  computed: {
    filteredItems() {
      return this.autocompleteItems.filter(i => {
        return i.text.toLowerCase().indexOf(this.tag.toLowerCase()) !== -1;
      });
    }
  }
});
app.addTagRemove();
app.getMentors();
app.getGroups();
app.fillFrom();

jQuery(document).on("click", ".ti-autocomplete", function(e){
  e.preventDefault();
  var clickedLi = $.trim($(e.target).text());
  app.checkIfGroup(clickedLi);
});

jQuery(document).on("click", "#zapri", function(e){
  location.reload();
});

var sav = [];
jQuery(document).ready(function($) {
  $('#addfile').change(function(){
    var filename = $(this)[0].files;
    if (filename.length == 1){
      console.log("prvi");
      if (!exists(filename[0].name)){
        sav.push(filename[0]);
      }
    }else{
      console.log("drugi");
      for(var i=0; i < filename.length; i++){
        if (!exists(filename[i].name)){
          sav.push(filename[i]);
        }
      }
    }
    console.log(sav)
    $("#seznamo").empty();
    for(var i = 0; i < sav.length; i++){
      // console.log("ime: " + sav[i]["name"] + " velikost: " + formatBytes(sav[i]["size"]));
      var n = sav[i]["name"];
      $("#seznamo").append('<li>' + '<label style="margin-top:2%">' + sav[i]["name"] + '</label>' + " (" + '<label class="lJ">' + formatBytes(sav[i]["size"]) + '</label>' + ")" + '<span class="close12" onclick="liklik(event)">&times;</span></li>');
    }
  });

  $('.fas fa-paperclip').tooltip;
  var quill = new Quill('#editor', {
    theme: 'snow'
  });
  var tab = sessionStorage.getItem("selected");
  if (tab != null){
    var splt = tab.split(",");
    for (let index = 0; index < splt.length; index++) {
      app.fillTags(splt[index]);
    }
    sessionStorage.removeItem("selected");
  }

});

function exists(username) {
  return sav.some(function(el) {
    return el.name === username;
  });
}

function formatBytes(bytes,decimals) {
  if(bytes == 0) return '0 Bytes';
  var k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function liklik(e){
  var name = e.target.parentElement.children[0].innerHTML;
  e.target.parentElement.style.display = 'none';
  sav = sav.filter(person => person.name != name.toString());
  console.log(sav);
}