Vue.use(VueTables.ClientTable);

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      currentSchoolName: "",
      currentSchoolId: "",
      currentUser:"",
      modalDate: "",
      default_school_name: "",
      edit_status: "",
      delete_status: "",
      adminEditSuccess: false,
      adminEditHasError: false,
      adminDeleteSuccess: false,
      adminDeleteHasError: false,
      schoolExists: true,

      columns: ['Naziv'],
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
      sortable: ['Ime_Tekmovanje'],
      filterable: ['Ime_Tekmovanje']
    },
    methods: {
      getSole: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
       axios
        .get("/api/upravitelj_tekmovanj/get_schools/", false,{headers: headers})
        .then((response) => {
          this.tableData=response.data;
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      },

      get_school(schoolId) {
        this.edit_status="";
        this.delete_status="";
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/upravitelj_tekmovanj/get_school/", {params: {schoolId:schoolId}}, {headers:headers})
        .then((response) => {
            if(response.status == 200) {
                this.schoolExists = true;
                this.currentSchoolName = response.data.Naziv;
                this.currentSchoolId = schoolId;
                this.currentUser = response.data.Ime_Uporabnika;
                let date = response.data.created_at.split("T")[0].split("-");
                this.modalDate = date[2]+"."+date[1]+"."+date[0];
                this.default_school_name = this.currentSchoolName;
            }
          })
        .catch(error => {
            if(error.response.status == 400) {  
              this.schoolExists = false;
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        },
        edit_school(schoolId) {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          if(this.currentSchoolName == this.default_school_name) {
            this.edit_status="Šoli morate spremeniti ime";
            this.adminEditSuccess=false;
            this.adminEditHasError=true;
            return 1;
          }
          axios
          .post("/api/upravitelj_tekmovanj/change_school_name/", {schoolId:schoolId,Naziv:this.currentSchoolName}, {headers:headers})
          .then((response) => {
              if(response.status == 201) {
                  this.edit_status = "Naziv šole uspešno spremenjen";
                  this.adminEditSuccess=true;
                  this.adminEditHasError=false;
              }
          })
          .catch(error => {
            if(error.response.status == 400) {
              this.edit_status="Šola, ki jo želite spremeniti ne obstaja";
              this.adminEditSuccess=false;
              this.adminEditHasError=true;
            }
            if(error.response.status == 409) {
              alert("Napaka pri procesiranju parametrov.");
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 405) {
              this.edit_status="Šola s tem imenom že obstaja";
              this.adminEditSuccess=false;
              this.adminEditHasError=true;
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        },

      delete_school(event, schoolId) {
        var result = confirm("Ste prepričani, da želite izbrisati šolo?");
            if (result) {
            var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
            axios
            .delete("/api/upravitelj_tekmovanj/delete_school/?schoolId="+ schoolId, {headers:headers})
            .then((response) => {
                if(response.status == 202) {
                    let button = document.getElementById(event.target.id);
                    button.style="background-color:green";
                    button.innerHTML="Šola izbrisana";
                    button.disabled=true;             
                    this.tableData = this.tableData.filter(x => x.ID_Sola !== schoolId);
                    this.adminDeleteSuccess=true;
                    this.adminDeleteHasError=false;
                }
            })
            .catch(error => {
            if(error.response.status == 404) {
                alert("Šola ne obstaja");
            }
            if(error.response.status == 409) {
              switch(error.response.data) {
                case 'no_parameters': this.delete_status="Za šolo ni parametrov"; break;
                case 'users_exists': this.delete_status="Na šoli učijo mentorji, zato te šole ne morete izbirsati."; break;
              }
              this.adminDeleteSuccess=false;
              this.adminDeleteHasError=true;
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                console.log("Something went wrong");
            }
            });
        }
      },
      downloadCsv(text, fileType, fileName) {
        var blob = new Blob([text], { type: fileType })
        var a = document.createElement('a');
        a.download = fileName;
        a.href = URL.createObjectURL(blob);
        a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
      },
      toCsv: function() {
        let newTable=this.tableData.filter(el => el !== null);
        this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
      },
    },
  });

app.getSole();
