Vue.use(VueTables.ClientTable);

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      currentMessage: {id:"",title:"",message:"",email:""},
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
      columns: ['title','created_at','izbris'],
    },
    methods: {
      get_messages: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
       axios
        .get("/api/upravitelj_tekmovanj/get_messages/", false,{headers: headers})
        .then((response) => {
          this.tableData=JSON.parse(response.data);
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      },

      delete_message(messageId) {
        var result = confirm("Ste prepričani, da želite izbrisati sporočilo?");
            if (result) {
            var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
            axios
            .delete("/api/upravitelj_tekmovanj/delete_message/?messageId="+messageId, {headers:headers})
            .then((response) => {
              this.tableData = this.tableData.filter(x => x.id !== messageId);
            })
            .catch(error => {
            if(error.response.status == 404) {
                alert("Šola ne obstaja");
            }
            if(error.response.status == 409) {
              alert("Manjkajoči parametri")
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                console.log("Something went wrong");
            }
            });
        }
      },
      get_message(idMessage) {
          let message = this.tableData.filter(msg => msg.id === idMessage)[0];
          this.currentMessage = {
              id: message.id,
              title: message.title,
              message: message.message,
              email: message.email
          }
      },
      downloadCsv(text, fileType, fileName) {
        var blob = new Blob([text], { type: fileType })
        var a = document.createElement('a');
        a.download = fileName;
        a.href = URL.createObjectURL(blob);
        a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
      },
      toCsv: function() {
        let newTable=this.tableData.filter(el => el !== null);
        this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
      },
    },
  });

app.get_messages();
