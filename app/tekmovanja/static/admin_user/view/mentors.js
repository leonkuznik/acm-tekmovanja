Vue.use(VueTables.ClientTable);

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data() {
      return{
        selectedUser:"",
        user_schools: "",
        tekmovanja: "",
        selected: [],
        users: [],
        groupTable: [],
        groups: [],
        selectAll: false,
        status: "",
        imeskup: "",
        groupName: "",
        stej:1,
        stej2:1,
        columns: ['checkbox','first_name','email'],
        tableData: [
        ],
        options: {
          texts: {
            filterPlaceholder:"Iskanje po imenu ali e-pošti...",
          },
          headings: {
            checkbox: '',
            first_name: "Ime in priimek",
            email: "e-pošta"
          },
          perPage: 10,
        }
      }
    },

    computed: {
      chechkGlob: function(){
        // pridobi admina
        if(localStorage.getItem('user')) {
          this.user = JSON.parse(localStorage.getItem('user'));
        } else {
          this.get_user_info(this.user.id);
        }
        return this.user.id;
      }
    },

    filters: {
      truncate: function (text, length, suffix) {
        if(length >= 15)
          return text.substring(0, (length/2)-5) + suffix;
        else
          return text
      },
    },

    methods: {
    getMentors: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/list_users/",false,{ headers: headers } )
      .then((response) => {
        if(response.status == 200) {
          var ne = sessionStorage.getItem("tab");
          if(ne == "null"){
            var nek = sessionStorage.getItem("tabela");
            this.tableData = JSON.parse(nek);
          }else{
            sessionStorage.setItem("tabela",JSON.stringify(response.data));
            this.tableData = response.data;
          }
          this.users = response.data;
          document.getElementById("len").innerHTML = this.tableData.length;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },
    getUser(userId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/get_user_by_id/",{params: { userId:userId}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          console.log(response);
          this.selectedUser = response.data;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    getUserSchoolsSubscriptions(userId) {
      var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
      axios
      .get("/api/upravitelj_tekmovanj/user_school_subscripitons_by_id/",{params: { userid:userId}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          this.user_schools = response.data;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

  getUserCompetitions(userId) {
    var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }

    axios
    .get("/api/upravitelj_tekmovanj/admin_view_user_competitions/",{params: { userid:userId}},{headers:headers})
    .then((response) => {
      if(response.status == 200) {
        this.tekmovanja = response.data;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  userConfirm: function(event,userId,schoolId) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }

    axios
    .post("/api/shared/confirm_user/", {userId:userId,schoolId:schoolId},{headers: headers})
    .then((response) => {
      let button = $(event.target);
      button.css("display","none");
      let status = button[0].parentElement.parentElement.childNodes[4];
      status.style="color:green";
      status.innerHTML='Potrjen';
    })
    .catch(error => {
      if(error.response.status == 400) {
        alert(error.response.data);
      }
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  select() {
    this.selected = [];
    if (!this.selectAll) {
      for (let i in this.tableData) {
        this.selected.push(this.tableData[i].id);
      }
    }
  },

  group:function(id){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    let naziv = document.getElementById(id).value;
    let koment = document.getElementById('coment').value;
    let koment2 = document.getElementById('coment1').value;
    // console.log("KOMENTAR1:");
    // console.log(koment);
    // console.log("KOMENTAR2:");
    // console.log(koment2);
    if(koment != ""){
      localStorage.setItem(naziv+"Koment",koment);
    }else if (koment2!=""){
      localStorage.setItem(naziv+"Koment",koment2);
    }
    this.groupName = naziv;
    // pridobi admina
    if(localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.get_user_info();
    }
    admin_id = this.user.id;
    global = 0;
    fun_or_const = 1;
    axios
    .post("/api/upravitelj_tekmovanj/admin_add_group_call/", {name:naziv, ad_id:admin_id, global:global, const:fun_or_const}, {headers:headers})
    .then((response) => {
        if(response.status == 201) {
            if (this.groupName == ""){
              this.status = "Vnos za skupino je prazen!";
              document.getElementById('status').style.color = "red";
              document.getElementById('status3').style.color = "red";
            }else{
              this.status = "Skupina je bila ustvarjena";
              this.groups.push({id:0, name:naziv});
              document.getElementById('status').style.color = "green";
              document.getElementById('status3').style.color = "green";
              // Kliči funkcijo za dodajanje userjev v grupe ali multiple posts
              if(id == "form2"){
                this.addUserGroup();
              }
              else{
                filter(1,1,naziv);
              }
            }
        }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 400) {
        this.status = "Prosimo vnesite ime skupine";
      }
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe prosimo prijavite se ponovno.");
      }
      if(error.response.status == 409) {
        this.status = "Skupina že obstaja";
        document.getElementById('status').style.color = "red";
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
        //this.status = "Pri dodajanju je prišlo do napake. Skupina morda že obstaja!";
      }
    });
  },

  addUserGroup:function(event){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/users_in_groups/",{groupName:this.groupName+"",groupsId:this.selected}, {headers: headers})
    .then(function(response) {
      if(response.status == 200) {
        console.log("OK");
        //console.log(this.selected.length)
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
    //this.getUsersInGroup();
  },

  getGroups: function() {
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    // pridobi admina
    if(localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.get_user_info();
    }
    admin_id = this.user.id;
    axios
    .get("/api/upravitelj_tekmovanj/get_groups/",{params: {idA:admin_id}},{headers:headers})
    .then((response) => {
      if(response.status == 200) {
        this.groups = response.data;
        this.groups.sort(compare);
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getUsersInGroup: function(nameGroup) {
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    if(nameGroup == "vsi"){
      $("#rez").css("display","none");
      $("#komentar").css("display","none");
      $("#filt").css("display","none");
      $("#skupina").css("display","none");
      this.groupTable = this.users;
      sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
      sessionStorage.setItem("tab",null);
      this.tableData = this.groupTable;
      document.getElementById("len").innerHTML = this.tableData.length;
    }else{
      if (localStorage.getItem(nameGroup+"Koment") == null) {
        $("#komentar").css("display","none");
      }
      axios
      .get("/api/upravitelj_tekmovanj/get_users_from_groups/",{params: {group_name:nameGroup}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          $("#rez").css("display","none");
          this.groupTable = [];
          for(i = 0; i< response.data.length; i++){
            var usersFilter =  this.users.filter(function(guser) {
              return guser.id == response.data[i].user_id_id;
            });
            this.groupTable.push(usersFilter[0]);
          }
          sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
          sessionStorage.setItem("tab",null);
          this.tableData = this.groupTable;
          document.getElementById("len").innerHTML = this.tableData.length;

          // ime filtriranja
          var grFilter =  this.groups.filter(function(guser1) {
            return guser1.name == nameGroup;
          });
          //console.log(grFilter[0].fun_or_const);
          //console.log(this.groups);
          if (grFilter[0].fun_or_const == 1){
            // Gre za elemente
            $("#skupina").css("display","inline-block");
            $("#filt").css("display","inline-block");
            $("#tekst").text("izbranih mentorjih v skupini")
            $("#skupina").text(nameGroup);
            $("#skupina").css("font-weight","Bold");
            $("#skupina").css("margin-left","6px");
            $("#filt").css("font-family","'Manjari', sans-serif");
            $("#filt").css("font-size","1.3em");
            //Če obstaja LocalStorage nameGroup+Koment
            //console.log(localStorage);
            if (localStorage.getItem(nameGroup+"Koment") != null) {
              $("#komentar").css("display","inline-block");
              var kk = localStorage.getItem(nameGroup+"Koment");
              $("#komentar").text(kk);
            }
          }else if(grFilter[0].fun_or_const == 0){
            // Gre za funkcijo
            $("#skupina").css("display","none");
            $("#filt").css("display","inline-block");
            // $("#filt").text("Filtriranje");
            var kateg = localStorage.getItem(nameGroup);
            //console.log(kateg);
            $("#tekst").text("funkciji in kategorijah: " + kateg + " v skupini " + nameGroup);
            $("#filt").css("font-family","'Manjari', sans-serif");
            $("#filt").css("font-size","1.3em");
            console.log(localStorage);
            if (localStorage.getItem(nameGroup+"Koment") != null) {
              $("#komentar").css("display","inline-block");
              var kk = localStorage.getItem(nameGroup+"Koment");
              $("#komentar").text(kk);
            }
          }
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    }
    //location.reload();
  },

  getSole: function() {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .get("/api/upravitelj_tekmovanj/get_schools/", false,{headers: headers})
    .then((response) => {
      //Populate type
      app.getTypes();
      populateSchools(response.data);
      document.getElementById('status2').style.display="none";
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getTekmovanja: function() {
    var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
    axios
    .get("/api/upravitelj_tekmovanj/get_competitions/", false,{headers: headers})
    .then((response) => {
      if(response.status == 200) {
        populateCompetitions(response.data);
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getCompetitionsOfType:function(type){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_competitions_of_type/", {params: {typeId:type}},{headers: headers})
    .then((response) => {
      if(response.status == 200) {
        populateCompetitionsWithType(response.data);
        //console.log(response.data);
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getTypes: function(){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_competitions_types/", false,{headers: headers})
    .then((response) => {
    if(response.status == 200) {
      populateCompetitionTypes(response.data);
      //console.log(response.data);
    }
    })
    .catch(error => {
    if(error.response.status == 403) {
      alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
    }
    if(error.response.status == 500) {
      console.log("Something went wrong");
    }
    });
  },

  getCMentors: function(nam) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .post("/api/upravitelj_tekmovanj/get_corfirmed_menthors/",{imeS:nam},{ headers: headers } )
    .then((response) => {
      if(response.status == 200) {
        // console.log(response.data);
        // console.log(this.users);
        //this.tableData = JSON.parse(response.data);
        this.groupTable = [];
        var oi = JSON.parse(response.data);
        for(i = 0; i< oi.length; i++){
          var usersFilter =  this.users.filter(function(guser) {
            return guser.id == oi[i].Mentor_id;
          });
          if (this.groupTable.includes(usersFilter[0]) == false){
            this.groupTable.push(usersFilter[0]);
          }
        }
        sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
        sessionStorage.setItem("tab",null);
        this.tableData = this.groupTable;
        document.getElementById("len").innerHTML = this.tableData.length;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getUMentors: function(nam) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .post("/api/upravitelj_tekmovanj/get_uncorfirmed_menthors2/",{imeS:nam},{ headers: headers } )
    .then((response) => {
      if(response.status == 200) {
        // console.log(response.data);
        // console.log(this.users);
        //this.tableData = JSON.parse(response.data);
        this.groupTable = [];
        var oi = JSON.parse(response.data);
        for(i = 0; i< oi.length; i++){
          var usersFilter =  this.users.filter(function(guser) {
            return guser.id == oi[i].Mentor_id;
          });
          // preveri duplikate
          if (this.groupTable.includes(usersFilter[0]) == false){
            this.groupTable.push(usersFilter[0]);
          }
        }
        sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
        sessionStorage.setItem("tab",null);
        this.tableData = this.groupTable;
        document.getElementById("len").innerHTML = this.tableData.length;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  usersOnSchool: function(event,nam) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .post("/api/upravitelj_tekmovanj/users_on_school/",{schoolsName:event, imeS:nam}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        this.groupTable = [];
        for(i = 0; i< response.data.length; i++){
          var usersFilter =  this.users.filter(function(guser) {
            return guser.id == response.data[i].ID_Mentor;
          });
          if (this.groupTable.includes(usersFilter[0]) == false){
            this.groupTable.push(usersFilter[0]);
          }
        }
        sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
        sessionStorage.setItem("tab",null);
        this.tableData = this.groupTable;
        document.getElementById("len").innerHTML = this.tableData.length;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  userOnCompetitions: function(event,nam) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .post("/api/upravitelj_tekmovanj/users_on_competitions/",{competitionsName:event,imeS:nam}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        this.groupTable = [];
        for(i = 0; i< response.data.length; i++){
          var usersFilter =  this.users.filter(function(guser) {
            return guser.id == response.data[i].ID_Mentor;
          });
          if (this.groupTable.includes(usersFilter[0]) == false){
            this.groupTable.push(usersFilter[0]);
          }
        }
        sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
        sessionStorage.setItem("tab",null);
        this.tableData = this.groupTable;
        document.getElementById("len").innerHTML = this.tableData.length;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  filterAllCombinations: function(g1,g2,g3,nam) {
    var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
    axios
    .post("/api/upravitelj_tekmovanj/filter_all_combinations/",{confirmed:g1+"",schools:g2,competitions:g3, imeS:nam}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        this.groupTable = [];
        for(i = 0; i< response.data.length; i++){
          var usersFilter =  this.users.filter(function(guser) {
            return guser.id == response.data[i].ID_Mentor;
          });
          if (this.groupTable.includes(usersFilter[0]) == false){
            this.groupTable.push(usersFilter[0]);
          }
        }
        sessionStorage.setItem("tabela",JSON.stringify(this.groupTable));
        sessionStorage.setItem("tab",null);
        this.tableData = this.groupTable;
        document.getElementById("len").innerHTML = this.tableData.length;
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  send: function(){
    sessionStorage.setItem("selected",this.selected);
    location.href = "/admin_send/";
  },

  editGroupName: function(){
    // Preveri robne pogoje!!!
    var placeh = document.getElementById("form3").placeholder;
    var field = document.getElementById("form3").value;
    if (field == ""){
      document.getElementsByClassName('state')[0].style.display="inline-block";
      document.getElementsByClassName('state')[0].style.color="red";
      document.getElementsByClassName('state')[0].style.marginLeft="35%";
      document.getElementsByClassName('state')[0].innerHTML = "Niste podali imena skupine!";
    }else{
      // Call API
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/upravitelj_tekmovanj/change_group_name/",{groupsName:field, placeH:placeh}, {headers: headers})
      .then((response) => {
        if(response.status == 200) {
          var project = this.groups.find((p) => {
            return p.name === placeh;
          });
          project.name = field;

          document.getElementsByClassName('state')[0].style.display="inline-block";
          document.getElementsByClassName('state')[0].style.color="green";
          document.getElementsByClassName('state')[0].style.marginLeft="30%";
          document.getElementsByClassName('state')[0].innerHTML = "Uspešno ste spremenili naziv skupine";
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 405) {
          document.getElementsByClassName('state')[0].style.display="inline-block";
          document.getElementsByClassName('state')[0].style.color="red";
          document.getElementsByClassName('state')[0].style.marginLeft="37%";
          document.getElementsByClassName('state')[0].innerHTML = "Ime skupine že obstaja!";
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    }
  },

  deleteGroupName: function(){
    var placeh = sessionStorage.getItem("value");
    var result = confirm("Ste prepričani, da želite izbrisati to skupino?");
    if (result) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/upravitelj_tekmovanj/delete_group/", {group_name:placeh},{headers:headers})
      .then((response) => {
          if(response.status == 202) {
            this.groups = $.grep(this.groups, function(item) {
              return item.name !== placeh;
            });

            localStorage.removeItem(placeh+"Koment");

            document.getElementsByClassName('state42')[0].style.display="inline-block";
            document.getElementsByClassName('state42')[0].style.color="green";
            document.getElementsByClassName('state42')[0].style.marginLeft="30%";
            document.getElementsByClassName('state42')[0].innerHTML = "Uspešno ste izbrisali to skupino";
          }
      })
      .catch(error => {
      if(error.response.status == 404) {
        document.getElementsByClassName('state42')[0].style.display="inline-block";
        document.getElementsByClassName('state42')[0].style.color="red";
        document.getElementsByClassName('state42')[0].style.marginLeft="37%";
        document.getElementsByClassName('state42')[0].innerHTML = "Ta skupina ne obstaja!";
      }
      if(error.response.status == 409) {
        document.getElementsByClassName('state42')[0].style.display="inline-block";
        document.getElementsByClassName('state42')[0].style.color="red";
        document.getElementsByClassName('state42')[0].style.marginLeft="37%";
        document.getElementsByClassName('state42')[0].innerHTML = "Napaka pri procesiranju parametrov.";
      }
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
          console.log("Something went wrong");
      }
      });
    }
  },

  getOsnovneSole: function(){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_elementary_schools/", false,{headers: headers})
    .then((response) => {
      console.log(response.data);
      populateSchools(response.data);
      document.getElementById('status2').style.display="none";
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  getSrednjeSole: function(){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_high_schools/", false,{headers: headers})
    .then((response) => {
      console.log(response.data);
      populateSchools(response.data);
      document.getElementById('status2').style.display="none";
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  get_user_info: function() {
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/get_user_info/", false, {headers:headers})
    .then((response) => {
      if(response) {
          localStorage.setItem('user', JSON.stringify(response.data.user));
          this.user = JSON.parse(localStorage.getItem('user'));
        }
    })
    .catch(error => {
      alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
    });
  },

  IfGlobal: function(skp){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/if_global/", {params: {name:skp}},{headers:headers})
    .then((response) => {
        // nastavi globalnost
        var globa = response.data[0]["glob"];
        if(globa == 0)
          $("#toggle-demo").bootstrapToggle('off');
        else
          $("#toggle-demo").bootstrapToggle('on');
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  setGlob: function(namee,gl){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/set_global/", {name:namee,globa:gl},{headers: headers})
    .then((response) => {
      if(gl == 0)
        $("#toggle-demo").bootstrapToggle('off');
      else
        $("#toggle-demo").bootstrapToggle('on');
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  modalUp:function(){
    $("#modalSubscriptionForm3").css("top","-5%");
    app.getSole();
  },

  modalDown:function(){
    $("#modalSubscriptionForm3").css("top","22%");
  },

  cmentorsView: function(name,state){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/cmentors_view/", {name:name, state:state},{headers: headers})
    .then((response) => {
      //console.log(response.data)
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 409) {
        $('#status3').innerHTML = "Skupina že obstaja";
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  usersOnSchoolView: function(sole,n){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/usr_on_school_view/", {school:sole, n:n + ""},{headers: headers})
    .then((response) => {
      console.log(response.data)
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 409) {
        $('#status3').innerHTML = "Skupina že obstaja";
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  userOnCompetitionsView: function(sol,nam){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/usr_on_competition_view/", {competit:sol, nam:nam + ""},{headers: headers})
    .then((response) => {
      console.log(response.data)
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 409) {
        $('#status3').innerHTML = "Skupina že obstaja";
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  filterAllCombinationsView: function(g1,g2,g3,n){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/filter_all_combinations_view/",{confirmed:g1+"",schools:g2,competitions:g3, name:n}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        console.log("ok")
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },


  fillUsersMent:function(){
    // console.log(nameogGroup);
    dat = "";
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_users_from_groups_all/",{params: {group_name:nameogGroup}}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        // Vsi k so v tej skupini
        console.log(response.data);
        if(this.stej == 1){
          var jsonData = JSON.stringify(response.data);
          $.each(JSON.parse(jsonData), function (idx, obj) {
            $('#select345').append($('<option>', {
              value: obj.id,
              text : obj.ime + " " + obj.priimek + " (" + obj.posta + ")"
            }));
            $('#select345').multipleSelect( 'refresh' );
          });
          this.stej = document.getElementById("select345");
          //$(".ms-select-all")[0].innerTXT = "<label><input type='checkbox' data-name='selectAll'><span>Izberi vse</span></label>";
        }
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  fillUsersDelMent: function(){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .get("/api/upravitelj_tekmovanj/get_users_from_groups_withData/",{params: {group_name:nameogGroup}}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        console.log(response.data);
        if(this.stej2 == 1){
          var jsonData = JSON.stringify(response.data);
          $.each(JSON.parse(jsonData), function (idx, obj) {
            $('#select346').append($('<option>', {
              value: obj.id,
              text : obj.ime + " " + obj.priimek + " (" + obj.posta + ")"
            }));
            $('#select346').multipleSelect( 'refresh' );
          });
          this.stej2 = document.getElementById("select346");
        }
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  addMentorToGroup:function(mentors,name){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    axios
    .post("/api/upravitelj_tekmovanj/add_mentors_to_group/",{mentors:mentors,imeS:name}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        // Vsi k so v tej skupini
        document.getElementById('status345').style.display="inline-block";
        document.getElementById('status345').style.color="green";
        document.getElementById('status345').style.marginLeft="36%";
        document.getElementById('status345').innerHTML = "Skupina uspešno posodobljena!";
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  deleteMentorInGroup: function(mentors2,name){
    var headers = {
      'X-CSRFToken': getCookie("csrftoken"),
    }
    console.log(mentors2);
    axios
    .post("/api/upravitelj_tekmovanj/delete_mentors_in_group/",{mentors:mentors2,imeS:name}, {headers: headers})
    .then((response) => {
      if(response.status == 200) {
        // Vsi k so v tej skupini
        document.getElementById('status346').style.display="inline-block";
        document.getElementById('status346').style.color="green";
        document.getElementById('status346').style.marginLeft="36%";
        document.getElementById('status346').innerHTML = "Skupina uspešno posodobljena!";
      }
    })
    .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
        console.log("Something went wrong");
      }
    });
  },

  downloadCsv(text, fileType, fileName) {
    var blob = new Blob([text], { type: fileType })
    console.log("PRIDE")
    var a = document.createElement('a');
    a.download = fileName;
    a.href = URL.createObjectURL(blob);
    a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    console.log("PRIDE2")
    document.body.removeChild(a);
    setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
  },
  toCsv: function() {
    let newTable=this.tableData.filter(el => el !== null);
    console.log(newTable);
    this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
  },

  handler(userId) {
    this.getUser(userId);
    this.getUserCompetitions(userId);
    this.getUserSchoolsSubscriptions(userId);
  }
  }});

app.getMentors();
app.getGroups();

jQuery(document).ready(function($) {
  // $('#table').dataTable( {
  //   "pagingType": "full_numbers"
  // } );

  schoolSelected = [];
  potrjenost = "";
  openNav();

  $('#uredi').tooltip();
  $('#brisi').tooltip();

  $('#nav-collapse').css("width","100%");
  $('.navbar-brand').css("display","none");
  $('#mailHome').css("display","inline-block");
  // $('#dodan').attr('href','/admin_index/');
  $('.openbtn').css("background","transparent");

  $(".table td").click(function() {
    var href = $(this).find("a").attr("href");
    if(href) {
        window.location = href;
    }
  });

  // $('input[type="checkbox"]').click(function(){
  //   if ($(".custom-control input:checkbox:checked").length > 0){
  //       $('.pokazi').css('display','inline-block')
  //          .css('padding-bottom','2%');
  //       $('#filtr').css('margin-top','2%').css('width','12%');
  //       $('#ment').css('outline','2px solid #4CAF50');
  //       $('#ment').tooltip();
  //   }else{
  //         $('.pokazi').css('display','none');
  //   }
  // });

  $(document).on("click","input[type='checkbox']", function(){
    if ($(".custom-control input:checkbox:checked").length > 0){

      $('.pokazi').css('display','inline-flex').css("margin-top", "0.3%");

      $('#filtr').css('margin-top','2.3%').css('width','26%');
      $('#ment').css('outline','2px solid #4CAF50');
      $('#export').css("margin-top","2%").css("margin-right","-16%");
      $('.pokazi').css("margin-left","-79px");
      $('#filtr').css("margin-right", "15%");

      // $('#ment').tooltip();
      // $("i[data-toggle='tooltip']").tooltip();
    }else{
          $('.pokazi').css('display','none');
          $('#export').css("margin-left","9%");
    }
  });

  $('.slectOne').on('change', function() {
    $('.slectOne').not(this).prop('checked', false);
    if($(this).is(":checked"))
      potrjenost = $(this).data( "id" );
    else
      potrjenost = "not";
  });

  // $('#select_option').on('change',function(){
  //   console.log(this.options);
  //   //this.perPage = parseInt(this.value);
  // });

  $('.slectOne1').on('change', function() {
    $('.slectOne1').not(this).prop('checked', false);
    if($(this).is(":checked")){
      rez = $(this).data( "id" );
      if(rez == "osnovne"){
        emptySchools();
        app.getOsnovneSole();
      }
      else if(rez == "srednje"){
        emptySchools();
        app.getSrednjeSole();
      }
    }else{
      emptySchools();
      app.getSole();
    }
  });

  $('.prik').css('display','inline-block');
  $('.dropdown-toggle').click();
  $('.dropdown').click(function(){
  $('.dropdown-menu').toggleClass('show');
  });

  $('#select1').multipleSelect({
    width: 290,
    filter: true
  })

  $('#select3').multipleSelect({
    width: 290,
    filter: true
  })

  $('#select2').multipleSelect({
    width: 290,
    filter: true
  })

  $('#select345').multipleSelect({
    width: 310,
    filter: true
  })

  $('#select346').multipleSelect({
    width: 290,
    filter: true
  })

  $('#select4').multipleSelect({
    width: 290,
    filter: true
  })

  //V tabeli dodaja chehkboc vsi
  // <input type="checkbox" class="custom-control-input" id="ment" v-model="selectAll" @click="select" title="Izberi vse"><label style="margin-right: -11%;">Vsi</label>
  // <label class="custom-control-label" for="ment"></label>
  $(".VueTables__sortable ")[0].innerHTML = "<input type='checkbox' class='custom-control-input' id='ment' v-model='selectAll' @click='select' title='Izberi vse'><span id='vs'>Vsi</span><label id='uredi' class='custom-control-label' for='ment'></label>";
  // $('.form-control')[3].chidren="<option value='8'>8</option>";
  $(new Option('8', '8')).prependTo('.form-control');
});
var nameogGroup = "";
jQuery(document).on("click", ".editGroup", function(e){
  var groupName = $(e.target).parent().closest(".dropdown-menu").children()[1].placeholder;
  //var groupName = $(e.target).parent().closest(".dropdown-menu").children()[0].outerText;
  nameogGroup = groupName;
  if (localStorage.getItem(nameogGroup+"Koment") != null) {
    $("#urediKoment").attr("placeholder", localStorage.getItem(nameogGroup+"Koment"));
  }else{
    $("#urediKoment").attr("placeholder", "Komentarja za to skupino še niste ustvarili!");
  }
  
  $("#form3").attr("placeholder", groupName);
  // nastavi globalno/ne globalno
  app.IfGlobal(groupName);
});

jQuery(document).on("click", ".editGroup", function(e){
  var groupName = $(e.target).parent().closest(".dropdown-menu").children()[1].placeholder;
  //var groupName = $(e.target).parent().closest(".dropdown-menu").children()[0].outerText;
  if (localStorage.getItem(groupName+"Koment") != null) {
    $("#urediKoment").attr("placeholder", localStorage.getItem(groupName+"Koment"));
  }else{
    $("#urediKoment").attr("placeholder", "Komentarja za to skupino še niste ustvarili!");
  }
  $("#form3").attr("placeholder", groupName);
});

jQuery(document).on("click", ".deleteGroup", function(e){
  var groupName = $(e.target).parent().closest(".dropdown-menu").children()[1].placeholder;
  //var groupName = $(e.target).parent().closest(".dropdown-menu").children()[0].outerText;
  sessionStorage.setItem("value",groupName);
});

jQuery(document).on("click", "#ccc", function(e){
  location.reload();
});

jQuery(document).on("click", "#ccc2", function(e){
  location.reload();
});

jQuery(document).on("click", "#ccc3345", function(e){
  location.reload();
});

jQuery(document).on("click", ".toggle.btn-xs", function(e){
  //nameogGroup
  var stanje = $(e.target).parent().closest(".dropdown-menu").children().prevObject.prevObject.prevObject[0].classList[1].split("-")[1];
  //console.log(stanje);
  if(stanje == "success")
    app.setGlob(nameogGroup,0);
  else if(stanje == "danger")
    app.setGlob(nameogGroup,1);
});

function compare(a, b) {
  const id1 = a.id;
  const id2 = b.id;

  let comparison = 0;
  if (id1 > id2) {
    comparison = 1;
  } else if (id1 < id2) {
    comparison = -1;
  }
  return comparison;
}


function openNav() {
  document.getElementById("mySidebar").style.width = "20%";
  document.getElementById("container").style.width = "62%";
  document.getElementById("container").style.marginLeft = "24%";
  document.getElementById('spremeni').style.width="100%";
  document.getElementsByClassName('table-responsive')[0].style.width = "100.4%";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("container").style.margin= "auto";
  document.getElementById("container").style.width= "95%";
  document.getElementById("container").style.marginTop= "30px";
  document.getElementById('spremeni').style.width="100%";
  document.getElementsByClassName('table-responsive')[0].style.width = "100.2%";
}

var table1Len = 1;
function populateSchools(data){
  if (table1Len == 1){
    var jsonData = JSON.stringify(data);
    let d = JSON.parse(jsonData);
    $.each(d, function (idx, obj) {
      //$("#select1").append('<option value="' + obj.ID_Sola + '">' + obj.Naziv + '</option>');
      //op += '<option value="' + obj.ID_Sola + '">' + obj.Naziv + '</option>';
      // console.log('<option value="' + obj.ID_Sola + '">' + obj.Naziv + '</option>');
      $('#select1').append($('<option>', {
        value: obj.ID_Sola,
        text : obj.Naziv
      }));

      $('#select3').append($('<option>', {
        value: obj.ID_Sola,
        text : obj.Naziv
      }));
    });

    $('#select1').multipleSelect( 'refresh' );
    $('#select3').multipleSelect( 'refresh' );

    table1Len = document.getElementById("select1");
    table1Len = document.getElementById("select3");
    app.getTekmovanja();
  }
}

function emptySchools(){
  $('#select1').empty().multipleSelect('refresh');
  $('#select2').empty().multipleSelect('refresh');
  $('#select3').empty().multipleSelect('refresh');
  $('#select4').empty().multipleSelect('refresh');
  table1Len = 1;
}

function populateCompetitions(data){
  var jsonData = JSON.stringify(data);
  $.each(JSON.parse(jsonData), function (idx, obj) {
    $('#select2').append($('<option>', {
      value: obj.ID_Tekmmovanje,
      text : obj.Ime_Tekmovanje
    }));
    $('#select2').multipleSelect( 'refresh' );

    $('#select4').append($('<option>', {
      value: obj.ID_Tekmmovanje,
      text : obj.Ime_Tekmovanje
    }));
    $('#select4').multipleSelect( 'refresh' );
  });
}

function populateCompetitionsWithType(data){
  //$('#select1').empty().multipleSelect('refresh');
  $('#select2').empty().multipleSelect('refresh');
  //$('#select3').empty().multipleSelect('refresh');
  $('#select4').empty().multipleSelect('refresh');
  var jsonData = JSON.stringify(data);
  $.each(JSON.parse(jsonData), function (idx, obj) {
    $('#select2').append($('<option>', {
      value: obj.ID_Tekmmovanje,
      text : obj.Ime_Tekmovanje
    }));
    $('#select2').multipleSelect( 'refresh' );

    $('#select4').append($('<option>', {
      value: obj.ID_Tekmmovanje,
      text : obj.Ime_Tekmovanje
    }));
    $('#select4').multipleSelect( 'refresh' );
  });
}

function populateCompetitionTypes(data){
  $('#tipTekmovanja').empty();
  $('#tipTekmovanja1').empty();
  var jsonData = JSON.stringify(data);
  $('#tipTekmovanja').append($('<option>', {
    value: "vsi",
    text : "Vsi"
  }));
  $('#tipTekmovanja1').append($('<option>', {
    value: "vsi",
    text : "Vsi"
  }));
  $.each(JSON.parse(jsonData), function (idx, obj) {
    $('#tipTekmovanja').append($('<option>', {
      value: obj.id,
      text : obj.naziv
    }));
    $('#tipTekmovanja1').append($('<option>', {
      value: obj.id,
      text : obj.naziv
    }));
  });
}

function filter(get,pog,naz){
  console.log(get);
  if (get == 1 && pog == 0)
    $("#modalSubscriptionForm5").modal('toggle');
    $("#modalSubscriptionForm2").modal('toggle');

  // console.log(naz);
  // Pridobi potjene/nepotrjene -> ob izbiri vrne vrednost ali not
  // Pridobi izbrane šole
  var choosenSchools = $('#select1').multipleSelect('getSelects', 'text');
  //Pridobi izbrana tekmovanja
  var choosenCompetitions = $('#select2').multipleSelect('getSelects', 'text');

  // .......... LOGIKA FILTRIRANJA .........//
  if((potrjenost == "not" || potrjenost == "") && choosenSchools.length == 0 && choosenCompetitions.length == 0){
    // uporabnik ni izbral nobenega filtra
    document.getElementById('status2').style.display="inline-block";
    document.getElementById('status2').style.color="red";
    document.getElementById('status2').style.marginLeft="28%";
    document.getElementById('status2').innerHTML = "Izbrali niste nobene skupine!";
    $("#knof").prop("disabled", true);
  }else{
    // SAMOSTOJNE TRANSAKCIJE
    // potrjeni/nepotrjeni
    if(potrjenost != "not" && choosenSchools.length == 0 && choosenCompetitions.length == 0){
      if(potrjenost == "potrjeni"){
        if(get == 0)
          app.getCMentors("");
        else
          localStorage.setItem(naz,"potrjeni");
          app.getCMentors(naz);
      }else if(potrjenost == "nepotrjeni"){
        if(get == 0)
          app.getUMentors("");
        else
          localStorage.setItem(naz,"nepotrjeni");
          app.getUMentors(naz);
      }
    }else if((potrjenost == "not" || potrjenost == "") && choosenSchools.length != 0 && choosenCompetitions.length == 0){
      // Vsi mentorji ki učijo na izbranih šolah
      if (get == 0)
        app.usersOnSchool(choosenSchools,"");
      else
        localStorage.setItem(naz,"sole na katerih ucijo mentorji");
        app.usersOnSchool(choosenSchools,naz);
    }else if((potrjenost == "not" || potrjenost == "") && choosenSchools.length == 0 && choosenCompetitions.length != 0){
      //Vsi mentroji ki so prijavljeni na izbrana tekmovanja
      if(get == 0)
        app.userOnCompetitions(choosenCompetitions,"");
      else
        localStorage.setItem(naz,"tekmovanja na katerih so mentorji prijavljeni");
        app.userOnCompetitions(choosenCompetitions,naz);
    }else{
      // TRANSAKCIJE S KOMBINACIJAMI SKUPIN
      if(get == 0){
        app.filterAllCombinations(potrjenost,choosenSchools,choosenCompetitions,"");
      }else{
        var skup1 = "";
        var skup2 = "";
        var skup3 = "";

        if(potrjenost != "not")
          skup1 = potrjenost + ", ";
        if(choosenSchools.length != 0)
          skup2 = "sole na katerih ucijo mentorji, ";
        if(choosenCompetitions.length != 0)
          skup3 = "tekmovanja na katerih so mentorji prijavljeni, ";

        localStorage.setItem(naz,skup1 + skup2 + skup3);
        app.filterAllCombinations(potrjenost,choosenSchools,choosenCompetitions,naz);
      }
    }

    $("#filt").css("display","none");
    $("#skupina").css("display","none");
    //Po končanem filtriranju pokaži sporočilo o uspešnosti
    // document.getElementById('status2').style.display="inline-block";
    // document.getElementById('status2').style.color="green";
    // document.getElementById('status2').style.marginLeft="36%";
    // document.getElementById('status2').innerHTML = "Filtriranje uspešno!";
    var skup1 = "";
    var skup2 = "";
    var skup3 = "";

    if(potrjenost != "not")
      skup1 = potrjenost + ", ";
    if(choosenSchools.length != 0)
      skup2 = "sole na katerih ucijo mentorji, ";
    if(choosenCompetitions.length != 0)
      skup3 = "tekmovanja na katerih so mentorji prijavljeni, ";

    if(get == 0){
      // zapri okno in prikaži rezultate
      $("#modalSubscriptionForm2").modal('toggle');
      $("#rez").css("display","inline-block");
      $("#rez").css("font-family","'Manjari', sans-serif");
      $("#rez").css("font-size","1.3em");
      var skp = skup1 + skup2 + skup3;
      // Odstrani vejico na konc
      $("#rez").text("Prikaz rezultatov filtriranja po kategorijah: " + skp.substring(0,skp.length - 2));
    }
  }
}

function filter2(){
  // Pridobi potjene/nepotrjene -> ob izbiri vrne vrednost ali not
  // Pridobi izbrane šole
  var choosenSchools = $('#select3').multipleSelect('getSelects', 'text');

  //Pridobi izbrana tekmovanja
  var choosenCompetitions = $('#select4').multipleSelect('getSelects', 'text');

  console.log("----------------------");
  console.log(potrjenost);
  console.log(choosenSchools);
  console.log(choosenCompetitions);

  // .......... LOGIKA FILTRIRANJA .........//
  if((potrjenost == "not" || potrjenost == "") && choosenSchools.length == 0 && choosenCompetitions.length == 0){
    // uporabnik ni izbral nobenega filtra
    document.getElementById('status3').style.display="inline-block";
    document.getElementById('status3').style.color="red";
    document.getElementById('status3').style.marginLeft="28%";
    document.getElementById('status3').innerHTML = "Izbrali niste nobene skupine!";
  }else{
    // SAMOSTOJNE TRANSAKCIJE
    // potrjeni/nepotrjeni
    if(potrjenost != "not" && choosenSchools.length == 0 && choosenCompetitions.length == 0){
      if(potrjenost == "potrjeni"){
        // ustvari pogled s potrjenimi
        //ustvari sejo s potrjenim
        localStorage.setItem(nameogGroup,"potrjen");
        app.cmentorsView(nameogGroup, 1);
        //app.getCMentors();
      }else if(potrjenost == "nepotrjeni"){
        // ustvari pogled s nepotrjenimi
        localStorage.setItem(nameogGroup,"nepotrjen");
        app.cmentorsView(nameogGroup, 0);
        //app.getUMentors();
      }
    }else if((potrjenost == "not" || potrjenost == "") && choosenSchools.length != 0 && choosenCompetitions.length == 0){
      // Vsi mentorji ki učijo na izbranih šolah
      //app.usersOnSchool(choosenSchools);
      localStorage.setItem(nameogGroup,"sole na katerih ucijo mentorji");
      app.usersOnSchoolView(choosenSchools,nameogGroup);
    }else if((potrjenost == "not" || potrjenost == "") && choosenSchools.length == 0 && choosenCompetitions.length != 0){
      //Vsi mentroji ki so prijavljeni na izbrana tekmovanja
      //app.userOnCompetitions(choosenCompetitions);
      localStorage.setItem(nameogGroup,"tekmovanja na katerih so mentorji prijavljeni");
      app.userOnCompetitionsView(choosenCompetitions,nameogGroup);
    }else{
      // TRANSAKCIJE S KOMBINACIJAMI SKUPIN
      var skup1 = "";
      var skup2 = "";
      var skup3 = "";

      if(potrjenost != "not")
        skup1 = potrjenost + ", ";
      if(choosenSchools.length != 0)
        skup2 = "sole na katerih ucijo mentorji, ";
      if(choosenCompetitions.length != 0)
        skup3 = "tekmovanja na katerih so mentorji prijavljeni, ";

      localStorage.setItem(nameogGroup,skup1 + skup2 + skup3);
      app.filterAllCombinationsView(potrjenost,choosenSchools,choosenCompetitions,nameogGroup);
      console.log("FILTRIRAJ");
    }

    //Po končanem filtriranju pokaži sporočilo o uspešnosti
    document.getElementById('status3').style.display="inline-block";
    document.getElementById('status3').style.color="green";
    document.getElementById('status3').style.marginLeft="32%";
    document.getElementById('status3').innerHTML = "Skupina uspešno posodobljena!";
  }
}

function resetM() {
  // nastavi session na vse userje!
  var nek = sessionStorage.getItem("tabela");
  console.log(nek);

  sessionStorage.setItem("tab","lala");
  app.getMentors();
}

function dodajanjeMentorjev(){
  var choosenMentors = $('#select345').multipleSelect('getSelects', 'value');
  console.log(nameogGroup);
  if (choosenMentors.length == 0){
    document.getElementById('status345').style.display="inline-block";
    document.getElementById('status345').style.color="red";
    document.getElementById('status345').style.marginLeft="33%";
    document.getElementById('status345').innerHTML = "Izbrali niste nobenega mentorja!";
  }else{
    app.addMentorToGroup(choosenMentors,nameogGroup);
  }
}

function odstranjevanjeMentorjev(){
  var choosenMentors2 = $('#select346').multipleSelect('getSelects', 'value');
  console.log(choosenMentors2);
  if (choosenMentors2.length == 0){
    document.getElementById('status346').style.display="inline-block";
    document.getElementById('status346').style.color="red";
    document.getElementById('status346').style.marginLeft="33%";
    document.getElementById('status346').innerHTML = "Izbrali niste nobenega mentorja!";
  }else{
    app.deleteMentorInGroup(choosenMentors2,nameogGroup);
  }
}

function editGroupComment(){
  var placeh = document.getElementById("form3").placeholder;
  var komt = document.getElementById("urediKoment").value;
  localStorage.setItem(placeh+"Koment",komt);
  document.getElementsByClassName('state')[0].style.display="inline-block";
  document.getElementsByClassName('state')[0].style.color="green";
  document.getElementsByClassName('state')[0].style.marginLeft="30%";
  document.getElementsByClassName('state')[0].innerHTML = "Uspešno ste spremenili komentar skupine";
}

$("#tipTekmovanja").change(function(){
  if($(this).val() == "vsi"){
    $('#select2').empty().multipleSelect('refresh');
    $('#select4').empty().multipleSelect('refresh');
    app.getTekmovanja();
  }else{
    //app.emptySchools();
    app.getCompetitionsOfType($(this).val());
  }  
});

$("#tipTekmovanja1").change(function(){
  if($(this).val() == "vsi"){
    $('#select2').empty().multipleSelect('refresh');
    $('#select4').empty().multipleSelect('refresh');
    app.getTekmovanja();
  }else{
    //app.emptySchools();
    app.getCompetitionsOfType($(this).val());
  }  
});