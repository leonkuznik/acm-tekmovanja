
var user = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
     user: "",
     isUnconfirmed: false,
     haveNewMessages: false,
     firstUnconfirmedExists: false,
  },
  methods: {
    onInit() {
      if(localStorage.getItem('user')) {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.obvestilaHandler();
      } else {
        this.get_user_info();
      }
    },
    obvestilaHandler() {
        this.uncorfirmedMentors();
        this.new_messages();
        this.new_first_unconfirmed();
    },
    get_user_info: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/user/get_user_and_change_last_login/", false, {headers:headers})
      .then((response) => {
        if(response) {
            localStorage.setItem('user', JSON.stringify(response.data.user));
            this.user = JSON.parse(localStorage.getItem('user'));
            this.obvestilaHandler();
          }
      })
      .catch(error => {
      });
    },
    uncorfirmedMentors: function() {
        var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/upravitelj_tekmovanj/uncorfirmed_mentors_exists/",{params: {last_login : this.user.last_login}}, {headers:headers})
        .then((response) => {
            if(response) {
               this.isUnconfirmed = response.data;
            }
        })
        .catch(error => {
        });
    },
    new_messages: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/new_messages/",{params: {last_login : this.user.last_login}}, {headers:headers})
      .then((response) => {
          if(response) {
              this.haveNewMessages=response.data;
          }
      })
      .catch(error => {
      });
    },
    new_first_unconfirmed: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/upravitelj_tekmovanj/get_first_uncorfirmed_exists/",false, {headers:headers})
      .then((response) => {
          if(response) {
              this.firstUnconfirmedExists=response.data;
          }
      })
      .catch(error => {
      });
    }
  },
});
user.onInit();