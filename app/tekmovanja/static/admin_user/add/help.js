var message = new Vue({
    delimiters: ['[[', ']]'],
    el: "#container",
    data: {
      title:"",
      message:"",
      status: "",
      maxCountMessage: 500,
      remainingCountMessage: 500,
      maxCountTitle: 40,
      remainingCountTitle: 40,
      statusSuccess : false,
      statusNotSuccess: false,
    },
    methods: {
      addHelp() {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          data = {
              title: this.title,
              description: this.message,
          }
          if (confirm('Ste prepričani, da želite dodati pomoč?')) {
            axios
            .post("/api/upravitelj_tekmovanj/add_help/", data,{headers: headers})
            .then((response) => {
              this.statusNotSuccess = false;
              this.statusSuccess = true;
              this.status = "Pomoč uspešno dodana";
            })
            .catch(error => {
              if(error.response.status == 400) {
                  this.statusSuccess = false;
                  this.statusNotSuccess = true;
                  if (error.response.data === 'predolgo sporočilo ali naslov') {
                      this.status = "Predolgo sporočilo ali naslov. Naslov(največ 50 znakov), sporočilo(nejveč 500 znakov).";
                  }
                  if (error.response.data === 'sporočilo ali naslov prazno') {
                      this.status = "Naslov ali sporočilo prazno. Obvezni so vsi podatki."
                  }
                  if (error.response.data === 'already_exists') {
                    this.status = "Pomoč z istim naslovom in opisom že obstaja."
                }
              }
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
          }
        },
        countdownMessage: function() {
          this.remainingCountMessage = this.maxCountMessage - this.message.length;
        },
        countdownTitle: function() {
          this.remainingCountTitle = this.maxCountTitle - this.title.length;
       }
  
  
    },
  });
  
  