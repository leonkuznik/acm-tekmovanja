Vue.use(VueTables.ClientTable);
var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        columns: ['Naziv'],
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
    },
    methods: {
        get_schools: function() {
            axios
            .get("/api/get_public_schools/",false)
            .then((response) => {
              if(response.status == 200) {
                this.tableData = response.data;
              }
            })
            .catch(error => {
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
          },
    }
});
app.get_schools();