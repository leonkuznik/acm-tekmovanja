var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        status: "",
    },
    methods: {
      send_token: function() {
        token=document.getElementById("token").innerHTML;
        let data = {
            'token': token,
        };
        this.status="Pošiljanje...";
        axios
        .post("/api/guest/send_lost_password_token/", data)
        .then((response) => {
            this.status="Geslo uspešno poslano. Novo geslo je bilo poslano na mail.";
        })
        .catch(error => {
            if(error.response.status == 404) {
                window.location.replace("/lost_password_verify/"+this.token);
            }
            if(error.response.status==400) {
                window.location.replace("/lost_password_verify/"+this.token);
            }
        });
      }
    },
  });
