var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      email: "",
      status: ""
    },
    methods: {
      send_lost_password_email: function() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        let data = {
            'email': this.email,
        };
        this.status="Pošiljanje...";
        axios
        .post("/api/guest/send_lost_password_email/", data, {headers:headers})
        .then((response) => {
                this.status="Email uspešno poslan";
        })
        .catch(error => {
            if(error.response.status == 404) {
                this.status="Email naslov ne obstaja";
            } else {
                this.status = "Prišlo je do napake";
            }
        });
      }
    },
  });
