from django.shortcuts import render
from django.contrib.auth.models import User, Group
from tekmovanja.models import *
from tekmovanja.serializers import *
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.utils import timezone
from rest_framework.permissions import IsAuthenticated
from rest_framework import permissions
from rest_framework.decorators import authentication_classes
from rest_framework.decorators import api_view, permission_classes
import simplejson as json
from datetime import datetime, date
from datetime import timedelta
import pytz
from django.conf import settings
from site_config import settings as project_settings
from django.db.models import Q
from django.http import JsonResponse
from rest_framework.permissions import BasePermission
from tekmovanja.api.guest import deleteUserIfNotVerifiedEmail, deleteUserIfNotVerifiedUsername
import string
import random
import os
import math
from django.shortcuts import redirect
from django.contrib import auth
from django.core.mail import send_mail

# Pravice za dostop do apija
class IsUser(BasePermission):
    """
    Allows access only to users.
    """
    def has_permission(self, request, view):
        if(request.user.is_superuser):
            return 0
        if(request.user.is_staff):
            return 0
        return 1

# nastavitve za elektronski naslov 
def setEmailSettings():
    settings.EMAIL_HOST= project_settings.EMAIL_PRIMARY_HOST
    settings.EMAIL_PORT= project_settings.EMAIL_PRIMARY_PORT
    settings.EMAIL_HOST_USER= project_settings.EMAIL_PRIMARY_USER
    settings.EMAIL_HOST_PASSWORD= project_settings.EMAIL_PRIMARY_PASWWORD

# generetor naključnih končnic url-jev
def randomStringDigits(stringLength=60):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))

#...........................................................
# API-ji ZA MENTORJA
#...........................................................

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def competition_login(request):
    competitionId = -1
    schoolsId = -1
    try:
        competitionId = int(request.data["competitionId"])
        schoolId = int(request.data["schoolId"])
        isCheck = request.data["isCheck"]
    except:
        return Response("required_parameters_missing_or_incorrect",status=400)
    try: 
        competition = Tekmovanje.objects.get(ID_Tekmovanje=competitionId)
    except:
        return Response("tekmovanje not found", status=400)
    
    login_date = str(competition.start_login_date)
    
    curr_year = int(datetime.today().strftime('%Y'))
    curr_month = int(datetime.today().strftime('%m'))
    curr_day = int(datetime.today().strftime('%d'))
    
    try:
        tmp = login_date.split("-")
        year = int(tmp[0])
        month = int(tmp[1])
        day = int(tmp[2])
    except:
        return Response("Napačen datum za začetek prijave (YY-MM-DD)", 400)

    if date(curr_year,curr_month,curr_day) < date(year,month,day):
        return Response("datum je manjši od datuma za začetek prijave",status=400)
    userId = request.user.id
    usr = User.objects.get(id=userId)
    
    #odjava ali prijava na tekmovanje
    if not isCheck:
        Sodeluje.objects.filter(ID_Mentor=userId, ID_Tekmovanje=competitionId, ID_Sola=schoolId).delete()
    else:
        sola=Sola.objects.get(ID_Sola=schoolId)
        if Sodeluje.objects.filter(ID_Mentor=usr, ID_Tekmovanje=competition, ID_Sola=schoolId).count() == 0:
            sodeluje = Sodeluje(ID_Mentor=usr, ID_Tekmovanje=competition,ID_Sola=sola)
            sodeluje.save()
    return Response(200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def sodeluje_tekmovanje(request):
    #naloži vsa tekmovanja
    sez_tek = Tekmovanje.objects.values()
    userId=  request.user.id
    #za vsako tekmovanje preveri ali je skrito
    for t in sez_tek:
        if Skritatekmovanja.objects.filter(ID_Tekmovanje=t["ID_Tekmovanje"],ID_Mentor=userId,Hidden=1).exists():
            t["hidden"]="1"
        else:
            t["hidden"]="0"
        t["tip_tekmovanja"] = tip_tekmovanje.objects.filter(id=t["tip_tekmovanja_id"]).values("naziv")[0]["naziv"]

    data=list(sez_tek)
    return JsonResponse(data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def hide_competition(request):
    try:
        competitionId = request.data["competitionId"]
    except:
        return Response("required parameters not found",status=400)
    try:
        tekmovanje = Tekmovanje.objects.get(ID_Tekmovanje=competitionId)
    except:
        return Response("Tekmovanje not found",status=400)

    user = request.user
    if Skritatekmovanja.objects.filter(ID_Tekmovanje=tekmovanje,ID_Mentor=user,Hidden=1).exists():
        return Response("Tekmovanje already hidden", status=400)

    Sodeluje.objects.filter(ID_Tekmovanje=competitionId,ID_Mentor=user.id).delete()
    Skritatekmovanja(ID_Tekmovanje=tekmovanje,ID_Mentor=user,Hidden=1).save()
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def un_hide_competition(request):
    try:
        competitionId = request.data["competitionId"]
    except:
        return Response("required parameters not found",status=400)
    try:
        tekmovanje = Tekmovanje.objects.get(ID_Tekmovanje=competitionId)
    except:
        return Response("Tekmovanje not found",status=400)
    user = request.user
    Skritatekmovanja.objects.filter(ID_Tekmovanje=tekmovanje,ID_Mentor=user,Hidden=1).delete()
    return Response(status=201)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_user_info(request):
    usr = request.user

    s = UserSerializer(usr,context={'request': request}).data

    return Response({"user":s})

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def get_user_and_change_last_login(request):
    userId = request.user.id
    usr = request.user
    s = UserSerializer(usr,context={'request': request}).data

    last_login=Lastlogin.objects.filter(user=userId).values("last_login")
    if last_login:
        last_login = last_login.first()["last_login"]
        Lastlogin.objects.filter(user=userId).values("last_login").update(last_login=timezone.now())
    else:
        Lastlogin(user=request.user).save()
        last_login = timezone.now()
    s["last_login"] = last_login
    return Response({"user":s})

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def user_school_subscripitons(request):
    userId = request.user.id
    mentorUciNaSolah = Uci.objects.filter(ID_Mentor=userId).values("ID_Sola")
    if not mentorUciNaSolah.exists():
        return Response(status=404)
    for sola in mentorUciNaSolah:
        if Uci.objects.filter(ID_Sola=sola["ID_Sola"],ID_Mentor=userId).values("Potrjen")[0]["Potrjen"] == 1:
            sola["Potrjen"]="1"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
            nepotrjeniNaSoli= Uci.objects.filter(ID_Sola=sola["ID_Sola"],Potrjen=0).values()
            counter=0
            for nepotrjen in nepotrjeniNaSoli:
                if User.objects.filter(id=nepotrjen["ID_Mentor_id"], is_active=1).exists() and not Potrditve.objects.filter(Zahteva_id=nepotrjen["ID_Mentor_id"],Potrditelj_id=userId,ID_Sola_potrditev_id=sola["ID_Sola"]).exists():
                    counter+=1
            sola["Nepotrjeni"] = counter
        else:
            sola["Potrjen"]="0"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
            sola["Nepotrjeni"] = 0

    data=list(mentorUciNaSolah)
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def user_has_unconfirmed_mentors(request,last_login):
    userId = request.user.id
    try:
        last_login = request.GET.get("last_login")
    except:
        return Response("required parameters not found",status=400)

    mentorUciNaSolah = Uci.objects.filter(ID_Mentor=userId,created_at__gte=last_login).values("ID_Sola")
    nepotrjeniNaSoli = []
    if not mentorUciNaSolah.exists():
        return Response(status=404)
    for sola in mentorUciNaSolah:
        #preveri ali je mentor na tej šoli potrjen
        if Uci.objects.filter(ID_Sola=sola["ID_Sola"],ID_Mentor=userId).values("Potrjen")[0]["Potrjen"] == 1:
            nepotrjeniNaSoli= Uci.objects.filter(ID_Sola=sola["ID_Sola"],Potrjen=0).values()

            for nepotrjen in nepotrjeniNaSoli:
                if User.objects.filter(id=nepotrjen["ID_Mentor_id"], is_active=1).exists() and not Potrditve.objects.filter(Zahteva_id=nepotrjen["ID_Mentor_id"],Potrditelj_id=userId,ID_Sola_potrditev_id=sola["ID_Sola"]).exists():
                    return Response(status=200)
    return Response(status=404)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def user_competition_subscripitons(request,competitionId):
    userId = request.user.id
    try:
        competitionId = int(request.GET.get("competitionId"))
    except:
        return Response("nepravilni ali manjkajoči parametri",status=409)
    try:
        tekmovanje = Tekmovanje.objects.get(ID_Tekmovanje=competitionId)
    except:
        return Response(status=404)
    mentorUciNaSolah = Uci.objects.filter(ID_Mentor=userId).values("ID_Sola")
    if not mentorUciNaSolah.exists():
        return Response(status=404)

    for sola in mentorUciNaSolah:
        if Uci.objects.filter(ID_Sola=sola["ID_Sola"],ID_Mentor=userId).values("Potrjen")[0]["Potrjen"] == 1:
            sola["Potrjen"]="1"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
            sola["Prijavljen"]=Sodeluje.objects.filter(ID_Mentor=userId, ID_Tekmovanje=competitionId,ID_Sola=sola["ID_Sola"]).exists()
        else:
            sola["Potrjen"]="0"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
    data=list(mentorUciNaSolah)
    return JsonResponse({"user_schools":data,"ime_Tekmovanja":tekmovanje.Ime_Tekmovanje}, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def user_competitions(request):
    userId=request.user.id
    if not Sodeluje.objects.filter(ID_Mentor=userId).exists():
        return Response(status=404)
    sez_tek=Sodeluje.objects.filter(ID_Mentor = userId).values("ID_Tekmovanje","ID_Sola")
    for mentor_sola in sez_tek:
        mentor_sola["Naziv"] = Tekmovanje.objects.filter(ID_Tekmovanje = mentor_sola["ID_Tekmovanje"]).values()[0]["Ime_Tekmovanje"]
        mentor_sola["Ime_sola"]=Sola.objects.filter(ID_Sola=mentor_sola["ID_Sola"]).values()[0]["Naziv"]

        data=list(sez_tek)
    return JsonResponse(data, safe=False,status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def competition_info(request):
    competitionId=-1
    try:
        competitionId=int(request.GET.get("competitionId"))
    except:
        return Response(status=409)
    tekmovanje=Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).values("ID_Tekmovanje","competition_date","start_login_date","url_razpis","kontaktni_naslov","dodatni_podatki","tip_tekmovanja_id","obdobje","competition_date_from","competition_date_to").first()
    if tekmovanje == None:
        return Response("competition_not_found",400)
    tekmovanje["tip"] = tip_tekmovanje.objects.filter(id=tekmovanje["tip_tekmovanja_id"]).values("naziv")[0]["naziv"]
    
    return JsonResponse(tekmovanje, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def send_message(request):
    try:
        msq = request.data['message']
        title = request.data['title']
    except:
        return Response("manjkajoči podatki", status=409)
    if(len(msq) == 0 or len(title) == 0):
        return Response("sporočilo ali naslov prazno", status=400)
    if len(msq) > 150 or len(title) > 40:
        return Response("predolgo sporočilo ali naslov", status=400)
    if Messages.objects.filter(user_id=request.user.id).count() > 10:
        return Response("preveč_sporočil", status=400)
    user=request.user
    Messages(title=title,message=msq,user_id=user).save()
    return Response(status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def school_users(request,schoolId):
    userId = request.user.id
    try:
        schoolId = int(request.GET.get("schoolId"))
    except:
        return Response("nepravilni ali manjkajoči parametri",status=400)
    if not Sola.objects.filter(ID_Sola=schoolId).exists():
        return Response("school_not_exists",404)
    #preveri ali user za to šolo sploh obstaja
    if not Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).exists():
        return Response("user_not_exists",status=404)
    #preveri ali je nepotrjen
    if Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId,Potrjen=0).exists():
        return Response("user_unconfirmed",status=400)

    mentorjiSola = Uci.objects.filter(~Q(ID_Mentor=userId),ID_Sola=schoolId).values("ID_Mentor")
    sola = Sola.objects.get(ID_Sola=schoolId)
    data=list()
    for mentor_id in mentorjiSola:
        mentor_sola={}
        if User.objects.get(id=mentor_id["ID_Mentor"]).is_active:
            if Uci.objects.filter(ID_Sola=schoolId,ID_Mentor=mentor_id["ID_Mentor"]).values("Potrjen")[0]["Potrjen"] == 1:
                mentor_sola["ID_Mentor"] = mentor_id["ID_Mentor"]
                mentor_sola["Potrjen"]="1"
                mentor_sola["Naziv_mentor"] = User.objects.filter(id=mentor_id["ID_Mentor"]).values("email")[0]["email"]
                data.append(mentor_sola)
            else:
                if Potrditve.objects.filter(ID_Sola_potrditev_id=schoolId,Potrditelj_id=userId,Zahteva_id=mentor_id["ID_Mentor"]).exists():
                    mentor_sola["ID_Mentor"] = mentor_id["ID_Mentor"]
                    mentor_sola["Ze_potrjen"] = "1"
                    mentor_sola["Potrjen"]="0"
                    mentor_sola["Naziv_mentor"] = User.objects.filter(id=mentor_id["ID_Mentor"]).values("email")[0]["email"]
                    data.append(mentor_sola)
                else:
                    mentor_sola["ID_Mentor"] = mentor_id["ID_Mentor"]
                    mentor_sola["Ze_potrjen"] = "0"
                    mentor_sola["Potrjen"]="0"
                    mentor_sola["Naziv_mentor"] = User.objects.filter(id=mentor_id["ID_Mentor"]).values("email")[0]["email"]
                    data.append(mentor_sola)
    return JsonResponse({"user_schools":data,"izbrana_sola":sola.Naziv}, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def school_unregistration(request):
    try:
        schoolId = int(request.data["schoolId"])
        userId = request.user.id
    except:
        return Response("nepravilni ali manjkajoči parametri",status=409)
    Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).delete()
    Sodeluje.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).delete()
    Potrditve.objects.filter(ID_Sola_potrditev_id=schoolId,Zahteva_id=userId).delete()

    return Response(status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsUser))
def new_competitions(request,last_login):
    try:

        last_login = request.GET.get("last_login")
    except:
        return Response("required parameters not found",status=400)

    if Tekmovanje.objects.filter(created_at__gte=last_login).exists():
            return Response(status=200)
    return Response(status=404)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def user_add_schools(request):
    try:
        izbrane_sole = request.data["izbrane_sole"]
        userId = request.user.id
    except:
        return Response(status=409)

    if not isinstance(izbrane_sole,list):
        return Response("schools not in list", status=409)
       
    user=User.objects.get(id=userId)
    for sola in izbrane_sole:
        sola=Sola.objects.filter(Naziv=sola)[0]
        if sola != "" and not Uci.objects.filter(ID_Mentor=user.id,ID_Sola=sola.ID_Sola).exists():
            u = Uci(Potrjen=0,ID_Mentor=user,ID_Sola=sola)
            u.save()

    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def delete_user(request):
    userId=request.user.id
    user=User.objects.get(id=userId)
    Potrditve.objects.filter(Zahteva_id=userId).delete()
    Potrditve.objects.filter(Potrditelj_id=userId).delete()
    Sodeluje.objects.filter(ID_Mentor=userId).delete()
    Lastlogin.objects.filter(user_id=userId).delete()
    email_merge.objects.filter(Child_mail=userId).delete()
    email_merge.objects.filter(Master_mail=userId).delete()
    Verifikacija.objects.filter(user_id=userId).delete()
    Skritatekmovanja.objects.filter(ID_Mentor=userId).delete()
    Messages.objects.filter(user_id=userId).delete()
    mail_user_groups.objects.filter(user_id=userId).delete()
    Uci.objects.filter(ID_Mentor=userId).delete()
    user.delete()
    auth.logout(request)
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def user_change_profile(request):
    try:
        username = request.data['UpIme']
        email = request.data['email']
        ime = request.data['Ime']
        priimek = request.data['Priimek']
    except:
        return Response("mankajoci_podatki",status=409)

    #if "@" in username:
    #    return Response(status=407)

    current_user = User.objects.filter(id=request.user.id)
    current_user_values = current_user.values().first()

    if len(username) > 40:
        return Response("predolgo_up_ime_ali_email",status=409)

    #delete user if not verified yet
    if current_user_values["username"] != username and User.objects.filter(username=username).exists():
        if deleteUserIfNotVerifiedUsername(username,3600) == False:
            return Response("up_ime_zasedeno",status=409)

    #CHANGE USER EMAIL 
    if current_user_values["email"] != username and current_user_values["email"] != email and len(email) > 0:
        usr = User.objects.get(id=request.user.id)

        #delete user previous attempts to change email
        user_email_change.objects.filter(user_id=request.user.id).delete()
        
        #check if email is already taken and user is active
        if User.objects.filter(email=email, is_active=1).exists():
            return Response("email_zaseden",status=409)
        
        #check if some other user is changing his email 
        if user_email_change.objects.filter(new_email=email).exists():
            token = user_email_change.objects.get(new_email=email)    
            naive = token.created_at.replace(tzinfo=None)
            seconds_from_registration=datetime.now().timestamp()-naive.timestamp()
            if seconds_from_registration > 3600:
                user_email_change.objects.filter(new_email=email).delete()
            else:
                return Response("email_zaseden",status=409)
        
        #check if email is already taken and user is not active yet
        if User.objects.filter(email=email, is_active=0).exists():
            if(deleteUserIfNotVerifiedEmail(email,3600) == True):
                #send mail and add email to temporary table
                rand = randomStringDigits(random.randrange(50, 60))
                while(user_email_change.objects.filter(token=rand).exists()):
                    rand = randomStringDigits(random.randrange(50, 60))
                token = user_email_change.objects.create(user_id=usr,new_email=email, token=rand)

                setEmailSettings()
                message="""Pozdravljeni. V spletno aplikaciji ACM tekmovanja ste zahtevali zamenjavo elektronske naslova. Za dokončanje zamenjave elektronskega naslova Vas prosimo, da 
        kliknete na naslednji url naslov: mentor.acm.si/change_email/"""+str(token.token)
                send_mail('ACM mail change', message, settings.EMAIL_HOST_USER,[email],fail_silently=False,)
            else:
                return Response("email_zaseden",status=409)
        else:
            #send mail and add email to temporary table
            rand = randomStringDigits(random.randrange(50, 60))
            while(user_email_change.objects.filter(token=rand).exists()):
                rand = randomStringDigits(random.randrange(50, 60))
            token = user_email_change.objects.create(user_id=usr, new_email=email, token=rand)
            
            setEmailSettings()
            message="""Pozdravljeni. V spletno aplikaciji ACM tekmovanja ste zahtevali zamenjavo elektronske naslova. Za dokončanje zamenjave elektronskega naslova Vas prosimo, da 
        kliknete na naslednji url naslov: mentor.acm.si/change_email/"""+str(token.token)
            send_mail('ACM mail change', message, settings.EMAIL_HOST_USER,[email],fail_silently=False,)
    

    if current_user_values["username"] == username or len(username) == 0:
        username = current_user_values["username"]

    if current_user_values["first_name"] == ime or len(ime) == 0:
        ime = current_user_values["first_name"]

    if current_user_values["last_name"] == priimek or len(priimek) == 0:
        priimek = current_user_values["last_name"]

    current_user.update(username=username,first_name=ime,last_name=priimek)

    if current_user_values["username"] != username and len(username) > 0:
        setEmailSettings()
        message="""
        Pozdravljeni.
        V spletni aplikaciji ACM tekmovanja ste uspešno spremenili svoje uporabniško ime. V aplikacijo se lahko prijavite z novim uporabniškim imenom.
        
        Vaše novo uporabniško ime: """ + username + """
        
        Ekipa ACM
        """

        send_mail('ACM username', message, settings.EMAIL_HOST_USER,[current_user_values["email"]],fail_silently=False,)
    
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsUser))
def user_change_password(request):
    try:
        geslo = request.data['geslo']
    except:
        return Response(status=409)
    if (len(geslo) < 5):
        return Response(status=409)
    u = User.objects.get(id=request.user.id)
    u.set_password(geslo)
    u.save()
    return Response(status=201)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_help(request):
    return Response(user_help.objects.values())

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def confirm_user(request):
    """
    A api that confirm user. If confirmer is admin then confirm it else if confirmer
    is user then add to counter of all confirmations. If counter of all
    confirmations is greater than half of mentors in school then user is confirmed.
    """

    try:
        userId = int(request.data["userId"])
        schoolId = int(request.data["schoolId"])
    except:
        return Response("Manjkajoči podatki", status=400)
    
    #preveri ali je potrditelj potrjen na tej šoli, kjer potrjuje
    if request.user.is_staff == 0:
        if not Uci.objects.filter(ID_Mentor=request.user.id,ID_Sola=schoolId,Potrjen=1).exists():
            return Response("Na tej šoli niste potrjeni", status=400)
    
    #preveri ali uporabnik, ki ga potrjujemo še uči na šoli
    if not Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).exists():
        return Response("Uporabnik ne uči na šoli, osvežite stran", status=400)

    #preveri ali je uporabnik, ki ga potrjujemo že potrjen
    if Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId,Potrjen=1).exists():
        return Response("Uporabnik že potrjen", status=400)

    #preveri ali je uporabnik, ki ga potrjujemo že potrjen iz naše strani
    if Potrditve.objects.filter(ID_Sola_potrditev_id=schoolId,Potrditelj_id=request.user.id,Zahteva_id=userId).exists():
        return Response("Uporabnika ste že potrdili", status=400)

    #preveri ali uporabnik potrjuje sam sebe
    if request.user.id == userId:
        return Response(status=203)

    #če potrdi upravitelj je uporabnik avtomatsko potrjen, sicer ga mora potrditi vsaj polovica drugih potrjenih mentorjev
    if request.user.is_staff == 1:
        p = Potrditve(ID_Sola_potrditev_id=schoolId,Potrditelj_id=request.user.id,Zahteva_id=userId)
        p.save()
        Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).update(Potrjen = 1)
    else:
        p = Potrditve(ID_Sola_potrditev_id=schoolId,Potrditelj_id=request.user.id,Zahteva_id=userId)
        p.save()

        #število potrjenih mentorjev na šoli
        st_mentorjev = Uci.objects.filter(ID_Sola_id = schoolId, Potrjen=1).count()

        #število potrditev, ki jih ima mentor katerega potrjujemo
        st_potrditev = Potrditve.objects.filter(Zahteva_id=userId,ID_Sola_potrditev_id=schoolId).count()

        #polovica potrjenih mentorjev na šoli
        polovica_mentorjev = int(math.ceil(st_mentorjev/2))
        
        if st_potrditev >= polovica_mentorjev:
            Uci.objects.filter(ID_Mentor=userId,ID_Sola=schoolId).update(Potrjen = 1)
            return Response(status=202)
    return Response(status=201)

