from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import BasePermission
from django.contrib.auth.models import User, Group
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from tekmovanja.serializers import *
from rest_framework import status
from django.db import connection
from tekmovanja.models import *
import simplejson as json
from django.conf import settings
from site_config import settings as project_settings
from django.core.mail import send_mass_mail
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
import os
import datetime
import json

# Pravice za dostop do apija
class IsStaffUser(BasePermission):
    """
    Allows access only to staff users.
    """
    def has_permission(self, request, view):
        return request.user.is_staff and request.user.is_active and not request.user.is_superuser

# nastavitve za elektronski naslov 
def setEmailSettings():
    settings.EMAIL_HOST= project_settings.EMAIL_PRIMARY_HOST
    settings.EMAIL_PORT= project_settings.EMAIL_PRIMARY_PORT
    settings.EMAIL_HOST_USER= project_settings.EMAIL_PRIMARY_USER
    settings.EMAIL_HOST_PASSWORD= project_settings.EMAIL_PRIMARY_PASWWORD

#...........................................................
# API-ji ZA UPRAVITELJA TEKMOVANJ
#...........................................................

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def list_users(request):
    data=list(User.objects.all().values("id","last_login","is_superuser","username","first_name","last_name","email","is_staff","is_active","date_joined"))
    res = [i for i in data if not (i['is_staff'] == True)]
    return JsonResponse(res, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_groups(request, idA):
    # data=list(mail_group.objects.all().values())
    # return JsonResponse(data, safe=False)
    try:
        idA = int(request.GET.get("idA"))
    except:
        return Response('data_missing',status=409)
    data = list(mail_group.objects.filter(created_by=idA).values() | mail_group.objects.filter(glob=1).values())
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_competitions(request):
    sez_tek = Tekmovanje.objects.values()
    for t in sez_tek:
        t["tip_tekmovanja"] = tip_tekmovanje.objects.filter(id=t["tip_tekmovanja_id"]).values("naziv")[0]["naziv"]

    data=list(sez_tek)
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_competition(request):
    competitionId=-1
    try:
        competitionId=int(request.GET.get("competitionId"))
    except:
        return Response(status=409)
    tekmovanje=Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).values().first()
    if tekmovanje == None:
        return Response("competition_not_found",400)
    user=User.objects.get(id=tekmovanje["created_by_id"])
    tekmovanje["Ime_Uporabnika"] = user.username
    tekmovanje["tip"] = tip_tekmovanje.objects.filter(id=tekmovanje["tip_tekmovanja_id"]).values("naziv")[0]["naziv"]
    tekmovanje["id_tip"] = tip_tekmovanje.objects.filter(id=tekmovanje["tip_tekmovanja_id"]).values("id")[0]["id"]
    
    return JsonResponse(tekmovanje, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_users_from_groups(request, group_name):
    groupName = ""
    try:
        groupName=str(request.GET.get("group_name"))
    except:
        return Response(status=409)
    idA = int(mail_group.objects.get(name=groupName).idSkupine)
    data=list(mail_user_groups.objects.filter(group_id=idA).values())
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_users_from_groups_all(request, group_name):
    groupName = ""
    try:
        groupName=str(request.GET.get("group_name"))
    except:
        return Response(status=409)
    idA = int(mail_group.objects.get(name=groupName).idSkupine)
    data=list(mail_user_groups.objects.filter(group_id=idA).values())
    #print("GLEJ SM")
    idMe = [d.get('user_id_id', None) for d in data]
    #print(idMe)
    dataVsi=list(User.objects.filter(is_staff=0).values())
    idVsi = [d.get('id', None) for d in dataVsi]
    #print(idVsi)

    lst3 = [value for value in idVsi if value not in idMe]
    #print(lst3)
    vsiAll = []
    for i in lst3:
        vsiAll.append({"id": User.objects.get(id=i).id, "name":User.objects.get(id=i).username, "ime":User.objects.get(id=i).first_name, "priimek":User.objects.get(id=i).last_name, "posta":User.objects.get(id=i).email})
    return JsonResponse(vsiAll, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_users_from_groups_withData(request, group_name):
    groupName = ""
    try:
        groupName=str(request.GET.get("group_name"))
    except:
        return Response(status=409)
    idA = int(mail_group.objects.get(name=groupName).idSkupine)
    data=list(mail_user_groups.objects.filter(group_id=idA).values())
    #print("GLEJ SM")
    idMe = [d.get('user_id_id', None) for d in data]
    #print(idMe)

    vsiAll = []
    for i in idMe:
        vsiAll.append({"id": User.objects.get(id=i).id, "name":User.objects.get(id=i).username, "ime":User.objects.get(id=i).first_name, "priimek":User.objects.get(id=i).last_name, "posta":User.objects.get(id=i).email})
    return JsonResponse(vsiAll, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def users_in_groups(request):
    groupName = ""
    groupsId = -1
    try:
        groupName = str(request.data["groupName"])
        groupsId = list(request.data["groupsId"])
    except:
        return Response(status=405)
    # id ustvarjene grupe
    group = mail_group.objects.get(name=groupName).idSkupine
    #print(group)
    for groupid in groupsId:
        usId = User.objects.get(id=groupid)
        authg = mail_group.objects.get(idSkupine=group)
        adduser = mail_user_groups(user_id=usId, group_id=authg)
        adduser.save()
    return Response(200)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_add_school_call(request):
    try:
        naziv=request.data["Naziv"]
        vrsta=request.data["vrsta"]
    except:
        return Response(status=409)

    if vrsta != 0 and vrsta != 1:
        return Response("invalid parameter", status=400)

    if len(naziv) == 0:
        return Response("naziv empty", status=400)

    if len(naziv) > 105:
        return Response("predolgi_naziv_sole", status=400)

    if Sola.objects.filter(Naziv=naziv).exists():
        return Response(status=409)
    else:
        sola = Sola(Naziv=naziv,Vrsta_Sola=vrsta,created_by=request.user)
        sola.save()
        return Response(sola.ID_Sola, status=201)


@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def bulk_school(request):
    try:
        vrsta=request.data["vrsta"]
        schools=request.data["schools"]
    except:
        return Response(status=409)
    count = 0
    if vrsta != 0 and vrsta != 1:
        return Response("invalid parameter", status=400)

    if not type(schools) is list:
        return Response("invalid parameter", status=400)

    #remove all empty data
    schools = list(filter(None, schools))
 
    if not schools:
        return Response("prazen_seznam", status=400)

    if len(schools) != len(set(schools)):
        return Response("podvojeni_podatki",status=400)

    for school in schools:
        if not Sola.objects.filter(Naziv=school).exists():
            if len(school) > 0 and len(school) <= 105:
                sola = Sola(Naziv=school.strip(),Vrsta_Sola=vrsta,created_by=request.user)
                sola.save()
                count+=1

    return Response(count, status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def add_admin_user(request):
    try:
        username = request.data['UpIme']
        email = request.data['email']
        ime = request.data['Ime']
        priimek = request.data['Priimek']
        geslo = request.data['geslo']
    except:
        return Response('data_missing',status=409)

    if "@" in username:
        return Response(status=407)

    if len(username) > 25 or len(email) > 40:
        return Response('too_long', status=409)

    if len(geslo) < 5:
        return Response('password_too_short', status=409)

    if User.objects.filter(email=email).exists():
        return Response('email_exists',status=409)

    if User.objects.filter(username=username).exists():
        return Response('up_ime_exists',status=409)

    user = User.objects.create_user(
        first_name  = ime,
        last_name   = priimek,
        username    = username,
        email       = email,
        password    = geslo,
        is_active   = True,
        is_staff    = 1
    )

    return Response(status=202)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def add_help(request):
    try:
        title = request.data["title"]
        description = request.data["description"]
    except:
        return Response('data_missing',status=409)
    if(len(description) == 0 or len(title) == 0):
        return Response("sporočilo ali naslov prazno", status=400)
    if len(description) > 500 or len(title) > 40:
        return Response("predolgo sporočilo ali naslov", status=400)
    if user_help.objects.filter(title=title,description=description).exists():
        return Response("already_exists", status=400)
    user_help(title=title,description=description).save()
    return Response(status=201)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_help(request, helpId):
    try:
        helpId = int(request.GET.get("helpId"))
    except:
        return Response('data_missing',status=409)
    user_help.objects.filter(id=helpId).delete()
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_add_competition_call(request):
    try: 
        naziv=request.data["Ime_Tekmovanje"]
        datum_prijave=request.data["datum_prijave"]
        dodatni_podatki=request.data["dodatni_podatki"]
        url_razpis=request.data["url_razpis"]
        kontaktni_podatki=request.data["kontaktni_podatki"]
        selected_type=int(request.data["selected_type"])
        obdobje = int(request.data["obdobje"])
    except:
        return Response("Manjkajoci ali nepravilni obvezni podatki",400) 

    if obdobje != 0 and obdobje != 1:
        return Response("Nepravilno obdobje (0 (dnevno) ali 1(od do))", 400)

    try:
        datetime.datetime.strptime(datum_prijave, '%Y-%m-%d')
    except:
        return Response("Incorrect data format, should be YY-MM-DD", 400)

    #dnevno obdobje
    if obdobje == 0:
        try:
            datum_tekmovanja=request.data["datum_tekmovanja"]
        except:
            return Response("Manjkajoci ali nepravilni obvezni podatki",400)

        if datum_tekmovanja == "":
            return Response("Manjkajoci obvezni podatki",400)

        try:
            datetime.datetime.strptime(datum_tekmovanja, '%Y-%m-%d')
        except:
            return Response("Incorrect data format, should be YY-MM-DD", 400)

    #daljše obdobje
    if obdobje == 1:
        try:
            datum_tekmovanja_od=request.data["datum_tekmovanja_od"]
            datum_tekmovanja_do=request.data["datum_tekmovanja_do"]
        except:
            return Response("Manjkajoci ali nepravilni obvezni podatki (datum_tekmovanja_od, datum_tekmovanja_do)",400)

        if datum_tekmovanja_od == "" or datum_tekmovanja_do == "":
            return Response("Manjkajoci obvezni podatki",400)

        try:
            datetime.datetime.strptime(datum_tekmovanja_od, '%Y-%m-%d')
            datetime.datetime.strptime(datum_tekmovanja_do, '%Y-%m-%d')
        except ValueError:
            return Response("Incorrect data format, should be YY-MM-DD", 400)
       
    if len(naziv) == 0 or len(dodatni_podatki) == 0 or datum_prijave == "" or len(url_razpis) == 0 or len(kontaktni_podatki) == 0 or selected_type == "":
        return Response("Manjkajoci obvezni podatki",400)
    if not tip_tekmovanje.objects.filter(id=selected_type).exists():
        return Response("Tip tekmovanja ne obstaja",400)
    tip_tekmovanja = tip_tekmovanje.objects.get(id=selected_type)
    
    if Tekmovanje.objects.filter(Ime_Tekmovanje=naziv).exists():
        return Response(status=409)
    else:
        if obdobje == 0:
            tekmovanje = Tekmovanje(Ime_Tekmovanje=naziv,obdobje=0, competition_date=datum_tekmovanja, competition_date_from = "1970-01-01", competition_date_to = "1970-01-01", start_login_date=datum_prijave,tip_tekmovanja=tip_tekmovanja,url_razpis=url_razpis,kontaktni_naslov=kontaktni_podatki,dodatni_podatki=dodatni_podatki,created_by=request.user)
        if obdobje == 1: 
            tekmovanje = Tekmovanje(Ime_Tekmovanje=naziv,obdobje=1, competition_date="1970-01-01", competition_date_from=datum_tekmovanja_od, competition_date_to=datum_tekmovanja_do, start_login_date=datum_prijave,tip_tekmovanja=tip_tekmovanja,url_razpis=url_razpis,kontaktni_naslov=kontaktni_podatki,dodatni_podatki=dodatni_podatki,created_by=request.user)
        tekmovanje.save()
        return Response(tekmovanje.ID_Tekmovanje, status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_add_group_call(request):
    try:
        naziv=request.data["name"]
        adId = request.data["ad_id"]
        glob = request.data["global"]
        const = request.data["const"]
    except:
        return Response('data_missing',status=409)
    if mail_group.objects.filter(name=naziv).exists():
        return Response(status=409)
    else:
        if naziv != "":
            mail_group(name=naziv,created_by=adId,glob=glob,fun_or_const=const).save()
        return Response(status=201)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_school(request):
    schoolId=-1
    try:
        schoolId = int(request.GET.get("schoolId"))
    except:
        return Response(status=409)
    if not Sola.objects.filter(ID_Sola=schoolId).exists():
        return Response("school not found",400)
    sola=Sola.objects.filter(ID_Sola=schoolId).values().first()
    user=User.objects.get(id=sola["created_by_id"])
    sola["Ime_Uporabnika"] = user.username
    return JsonResponse(sola, safe=False)


@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def change_school_name(request):
    schoolId=-1
    try:
        schoolId=int(request.data["schoolId"])
        name=request.data["Naziv"]
    except:
        return Response(status=409)
        
    if not Sola.objects.filter(ID_Sola=schoolId).exists():
        return Response("school not found",status=400)
    if Sola.objects.filter(Naziv=name).exists():
        return Response(status=405)
    staro = Sola.objects.filter(ID_Sola=schoolId).values("Naziv")[0]["Naziv"]
    Sola.objects.filter(ID_Sola=schoolId).update(Naziv=name)

    #todo Obvesti mentorje o spremembi naziva šole
    sav = []
    idSole = Sola.objects.get(Naziv=name).ID_Sola
    ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
    ment11 = list(ment)
    if len(ment11) == 1:
        sav.append(User.objects.filter(id=ment11[0]["ID_Mentor"]).values("email")[0]["email"])
    else:
        for i in range(0,len(ment11)):
            sav.append(User.objects.filter(id=ment11[i]["ID_Mentor"]).values("email")[0]["email"])

    subject = "Sprememba naziva šole"
    message = "Spoštovani, \n\nobveščamo Vas, da se je šoli "+ staro + " na kateri učite naziv spremenil v " + name + "." + "\n\nLep pozdrav, Admin"
    from_email = 'sp7733@student.uni-lj.si'
    messages = [(subject, message, from_email, [recipient]) for recipient in sav]
    
    setEmailSettings()

    send_mass_mail(messages)
    return Response(status=201)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_school(request, schoolId):
    try:
        schoolId = int(request.GET.get("schoolId"))
    except:
        return Response("no_parameters",status=409)

    if not Sola.objects.filter(ID_Sola=schoolId).exists():
        return Response(status=404)

    if Uci.objects.filter(ID_Sola=schoolId).count() > 0:
        return Response("users_exists",status=409)

    ime = Sola.objects.filter(ID_Sola=schoolId).values("Naziv")[0]["Naziv"]
    # #obveščanje mentorjev o izbrisu šole
    # sav = []
    # ment = Uci.objects.filter(ID_Sola=schoolId).values("ID_Mentor")
    # ment11 = list(ment)
    # if len(ment11) == 1:
    #     sav.append(User.objects.filter(id=ment11[0]["ID_Mentor"]).values("email")[0]["email"])
    # else:
    #     for i in range(0,len(ment11)):
    #         sav.append(User.objects.filter(id=ment11[i]["ID_Mentor"]).values("email")[0]["email"])

    # subject = "Izbris šole"
    # message = "Spoštovani, \n\nobveščamo Vas, da je bila šola z nazivom " + ime + " izbrisana." + "\n\nLep pozdrav, Admin"
    # from_email = 'sp7733@student.uni-lj.si'
    # messages = [(subject, message, from_email, [recipient]) for recipient in sav]

    # send_mass_mail(messages)

    #TODOO : brisanje šol NE DOVOLIMO!! razen če ni nobenga mentorja, ki uči na tej šoli!

    Sola.objects.filter(ID_Sola=schoolId).delete()
    return Response(status=202)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def change_competition(request):
    competitionId=-1
    try:
        competitionId=int(request.data["competitionId"])
        naziv = request.data["Naziv"]
        datum_prijave = request.data["datum_prijave"]
        url_razpis = request.data["url_razpis"]
        kontaktni_naslov = request.data["kontaktni_naslov"]
        dodatni_podatki = request.data["dodatni_podatki"]
        selected_type = int(request.data["selected_type"])
        obdobje = int(request.data["obdobje"])
    except:
        return Response(status=409)
    
    changed = True

    #preveri tip tekmovanja
    if not tip_tekmovanje.objects.filter(id=selected_type).exists():
        return Response("Tip tekmovanja ne obstaja",400)
    #preveri obdobje (input data)
    if obdobje != 0 and obdobje != 1:
        return Response("Nepravilno obdobje (0 (dnevno) ali 1(od do))", 400)
    
    #preveri ali tekmovanje že obstaja
    if not Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).exists():
        return Response("competition not exists",400)
    
    #preveri format datuma  
    try:
        datetime.datetime.strptime(datum_prijave, '%Y-%m-%d')
    except ValueError:
        return Response("Incorrect data format, should be YY-MM-DD", 400)
   
    #preveri ali tekmovanje s tem imenom in tipom, ki ga želimo spremeniti že obstaja
    if Tekmovanje.objects.filter(~Q(ID_Tekmovanje = competitionId), Ime_Tekmovanje=naziv, tip_tekmovanja_id=selected_type).exists():
        return Response("tekmovanje s tem imenom in tipom že obstaja", status=400)
    
    if naziv == "" or datum_prijave == "" or url_razpis == "" or kontaktni_naslov == "" or dodatni_podatki == "":
        return Response("data missing", status=400)
    
    staro = Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).values()[0]
 
    #najprej preveri del vhodnih podatkov, če so se spremenili (naslednji del preverjanja zavisi od obdobja)
    if staro["Ime_Tekmovanje"] == naziv and staro["obdobje"] == obdobje and str(staro["start_login_date"]) == str(datum_prijave) and staro["url_razpis"] == url_razpis and staro["kontaktni_naslov"] == kontaktni_naslov and staro["dodatni_podatki"] == dodatni_podatki and staro["tip_tekmovanja_id"] == selected_type:
        changed = False
  
    #dnevno obdobje
    if obdobje == 0:
        try:
            datum_tekmovanja=request.data["datum_tekmovanja"]
        except:
            return Response("Manjkajoci ali nepravilni obvezni podatki",400)
        
        if datum_tekmovanja == "":
            return Response("data_missing",400)
        try:
            datetime.datetime.strptime(datum_tekmovanja, '%Y-%m-%d')
        except:
            return Response("Incorrect data format, should be YY-MM-DD", 400)
        #preveri naslednji del podatkov, če so se spremenili
        if changed == False and str(staro["competition_date"]) == str(datum_tekmovanja):
            return Response("no_changes",status=400)
        
    #daljše obdobje
    if obdobje == 1:
        try:
            datum_tekmovanja_od=request.data["datum_tekmovanja_od"]
            datum_tekmovanja_do=request.data["datum_tekmovanja_do"]
        except:
            return Response("Manjkajoci ali nepravilni obvezni podatki (datum_tekmovanja_od, datum_tekmovanja_do)",400)
        
        if datum_tekmovanja_od == "" or datum_tekmovanja_do == "":
            return Response("data_missing",400)

        try:
            datetime.datetime.strptime(datum_tekmovanja_od, '%Y-%m-%d')
            datetime.datetime.strptime(datum_tekmovanja_do, '%Y-%m-%d')
        except ValueError:
            return Response("Incorrect data format, should be YY-MM-DD", 400)
        

        #preveri naslednji del podatkov, če so se spremenili
        if changed == False and str(staro["competition_date_from"]) == str(datum_tekmovanja_od) and str(staro["competition_date_to"]) == str(datum_tekmovanja_do):
            return Response("no_changes",status=400)

    if staro["Ime_Tekmovanje"] != naziv: 
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(Ime_Tekmovanje=naziv)
    if staro["start_login_date"] != datum_prijave:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(start_login_date=datum_prijave)
    if staro["url_razpis"] != url_razpis:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(url_razpis=url_razpis)
    if staro["kontaktni_naslov"] != kontaktni_naslov:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(kontaktni_naslov=kontaktni_naslov)
    if staro["dodatni_podatki"] != dodatni_podatki:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(dodatni_podatki=dodatni_podatki)
    if staro["tip_tekmovanja_id"] != selected_type:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(tip_tekmovanja=selected_type)
    if staro["obdobje"] != obdobje:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(obdobje=obdobje)
    if obdobje == 0:
        if staro["competition_date"] != datum_tekmovanja:
            Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date_from="1970-01-01")
            Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date_to="1970-01-01")
            Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date=datum_tekmovanja)
    if obdobje == 1:
        Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date="1970-01-01")
        if staro["competition_date_from"] != datum_tekmovanja_od:
            Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date_from=datum_tekmovanja_od)
        if staro["competition_date_to"] != datum_tekmovanja_do:
            Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).update(competition_date_to=datum_tekmovanja_do)

    # Pridobi vse mentorje, ki so prijavljeni na to tekmovanje in jim posreduj e-pošto o spremembi imena tekmovanja
    if staro["Ime_Tekmovanje"] != naziv:
        sav = []
        idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=request.data["Naziv"]).ID_Tekmovanje
        ment1 = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
        ment11 = list(ment1)
        if len(ment11) == 1:
            sav.append(User.objects.filter(id=ment11[0]["ID_Mentor"]).values("email")[0]["email"])
        else:
            for i in range(0,len(ment11)):
                sav.append(User.objects.filter(id=ment11[i]["ID_Mentor"]).values("email")[0]["email"])

        subject = "Sprememba naziva tekmovanja"
        message = "Spoštovani, \n\nobveščamo Vas, da se je tekmovanju "+ staro["Ime_Tekmovanje"] + " na katerega ste prijavljeni naziv spremenil v " + request.data["Naziv"] + "." + "\n\nLep pozdrav, Admin"
        from_email = 'sp7733@student.uni-lj.si'
        messages = [(subject, message, from_email, [recipient]) for recipient in sav]
        
        setEmailSettings()

        send_mass_mail(messages)

    return Response(status=202)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_competition(request, competitionId):
    try:
        competitionId = int(request.GET.get("competitionId"))
    except:
        return Response("competiton_id_not_set",status=409)

    if not Tekmovanje.objects.filter(ID_Tekmovanje=competitionId).exists():
        return Response(status=404)

    if Sodeluje.objects.filter(ID_Tekmovanje=competitionId).count() > 0:
        return Response("users_participate",status=409)

    # ime = Tekmovanje.objects.filter(ID_Tekmovanje = competitionId).values("Ime_Tekmovanje")[0]["Ime_Tekmovanje"]

    # #obveščanje mentorjev o izbrisu tekmovanja
    # sav = []
    # ment1 = Sodeluje.objects.filter(ID_Tekmovanje=competitionId).values("ID_Mentor")
    # ment11 = list(ment1)
    # if len(ment11) == 1:
    #     sav.append(User.objects.filter(id=ment11[0]["ID_Mentor"]).values("email")[0]["email"])
    # else:
    #     for i in range(0,len(ment11)):
    #         sav.append(User.objects.filter(id=ment11[i]["ID_Mentor"]).values("email")[0]["email"])

    # subject = "Izbris tekmovanja"
    # message = "Spoštovani, \n\nobveščamo Vas, da je bilo tekmovanje z imenom " + ime + " izbrisano." + "\n\nLep pozdrav, Admin"
    # from_email = 'sp7733@student.uni-lj.si'
    # messages = [(subject, message, from_email, [recipient]) for recipient in sav]

    # send_mass_mail(messages)
    Skritatekmovanja.objects.filter(ID_Tekmovanje=competitionId).delete()
    Sodeluje.objects.filter(ID_Tekmovanje=competitionId).delete()
    Tekmovanje.objects.filter(ID_Tekmovanje = competitionId).delete()
    return Response(status=202)


@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_uncorfirmed_menthors(request):
    cursor = connection.cursor()

    cursor.execute('''SELECT auth_user.id as Mentor_id,email,tekmovanja_sola."ID_Sola" as Sola_id,tekmovanja_sola."Naziv" as naziv FROM auth_user,"Uci" as uci,tekmovanja_sola
                        WHERE uci."ID_Mentor" = auth_user.id and
                        tekmovanja_sola."ID_Sola" = uci."ID_Sola" and
                        uci."Potrjen" = 0''')
    rows = cursor.fetchall()

    result = []
    keys = ('Mentor_id','email','Sola_id','naziv')
    for row in rows:
        result.append(dict(zip(keys,row)))

    json_data = json.dumps(result)
    return JsonResponse(json_data, safe=False)


@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_menthors_by_competition(request, competitionId):
    try:
        competitionid = int(request.GET.get("competitionId"))
    except:
        return Response("Nepravilen ali manjkajoči parameter", status=409)
        
    if not Tekmovanje.objects.filter(ID_Tekmovanje = competitionid).exists():
        return Response(status=404)

    cursor = connection.cursor()
    cursor.execute(''' SELECT auth_user.email, tekmovanja_sola."Naziv" FROM "Sodeluje" as sodeluje, auth_user, tekmovanja_sola
                        WHERE sodeluje."ID_Mentor" = auth_user.id
                        AND sodeluje."ID_Sola" = tekmovanja_sola."ID_Sola"
                        AND "ID_Tekmovanje" = %s; ''', [competitionid])
    
    result = list()
    rows = cursor.fetchall()
    keys = ('email','naziv')
    for row in rows:
        result.append(dict(zip(keys,row)))

    json_data = json.dumps(result)
    return JsonResponse(json_data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_uncorfirmed_menthors_by_school(request):
    try:
        schoolId = int(request.GET.get("schoolId"))
    except:
        return Response("Nepravilen ali manjkajoči parameter", status=409)

    result = list()
    keys = ('Mentor_id','email')
    row=()
    uci=Uci.objects.filter(Potrjen=0,ID_Sola=schoolId).values()

    for uc in uci:
        user=User.objects.get(id=uc["ID_Mentor_id"])
        row=(user.id,user.email)
        result.append(dict(zip(keys,row)))

    json_data = json.dumps(result)
    return JsonResponse(json_data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_first_uncorfirmed_menthor(request):
    cursor = connection.cursor()

    #pridobi vse šole na katerih so nepotrjeni mentorji
    cursor.execute(''' SELECT sola."ID_Sola", sola."Naziv" FROM tekmovanja_sola as sola, "Uci" as uci
            WHERE sola."ID_Sola" = uci."ID_Sola"
            AND uci."Potrjen" = 0
            GROUP BY (sola."ID_Sola"); ''')

    sole = cursor.fetchall()
    result = list()
    keys = ('ID_Sola','Naziv')

    #Izberi vse šole, ki imajo manj kot 1 potrjenega mentorja
    for (id,naziv) in sole:
        if Uci.objects.filter(ID_Sola=id,Potrjen=1).count() < 1:
            result.append(dict(zip(keys,(id,naziv))))

    json_data = json.dumps(result)

    return JsonResponse(json_data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_first_uncorfirmed_exists(request):
    cursor = connection.cursor()

    #pridobi vse šole na katerih so nepotrjeni mentorji
    cursor.execute(''' SELECT sola."ID_Sola" FROM tekmovanja_sola as sola, "Uci" as uci
            WHERE sola."ID_Sola" = uci."ID_Sola"
            AND uci."Potrjen" = 0
            GROUP BY (sola."ID_Sola"); ''')

    sole = cursor.fetchall()

    #Izberi vse šole, ki imajo manj kot 1 potrjena mentorja
    for (id) in sole:
        if Uci.objects.filter(ID_Sola=id,Potrjen=1).count() < 1:
            return Response(True)
    return Response(False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_schools(request):
    data=list(Sola.objects.all().values())
    return JsonResponse(data,safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_competitions_types(request):
    data=list(tip_tekmovanje.objects.all().values())
    return JsonResponse(data,safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_uncorfirmed_menthors2(request):
    imeS = request.data["imeS"]
    result = list()
    keys = ('Mentor_id','email','Sola_id','naziv')
    row=()
    uci=Uci.objects.filter(Potrjen=0).values()
    for uc in uci:
        user=User.objects.get(id=uc["ID_Mentor_id"])
        sola=Sola.objects.get(ID_Sola=uc["ID_Sola_id"])
        row=(user.id,user.email,uc["ID_Sola_id"],sola.Naziv)
        result.append(dict(zip(keys,row)))

    json_data = json.dumps(result)

    if imeS != "":
        # Ob kliku pri filtriranju ustvari skupino in pogled iz teh mentorjev!
        idMe = [d.get('Mentor_id', None) for d in result]
        idMe = list(dict.fromkeys(idMe))
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        cursor = connection.cursor()

        se = ""
        if len(idMe)>1:
            se = tuple(idMe)
        else:
            se = str(idMe[0])

        my_dict = {
            'gname': imeS,
            'skp': se
        }
        if len(idMe) == 1:
            #SQL za posobljen pogled oz. novo skupino
            print("KAKAKA : ")
            print(my_dict["skp"])
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(skp)s
                                ''',my_dict)
        else:
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

        # Vstavi uporabnike v dejansko bazo oz. novo skupino
        # id ustvarjene grupe
        group = mail_group.objects.get(name=imeS).idSkupine
        # print(group)
        # print(groupsId)
        for groupid in idMe:
            usId = User.objects.get(id=groupid)
            authg = mail_group.objects.get(idSkupine=group)
            adduser = mail_user_groups(user_id=usId, group_id=authg)
            adduser.save()

    return JsonResponse(json_data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_corfirmed_menthors(request):
    imeS = request.data["imeS"]

    # cursor = connection.cursor()
    # cursor.execute('''SELECT user.id,user.email, sola."ID_Sola",sola."Naziv" FROM auth_user as user, "Uci" as uci, tekmovanja_sola as sola
    #                 WHERE uci."ID_Mentor" = user.id and
    #                 sola."ID_Sola" = uci."ID_Sola" and
    #                 user.is_staff = 0 and
    #                 uci."Potrjen" = 1 ''')

    # rows = cursor.fetchall()
    # result = []
    # keys = ('Mentor_id','email','Sola_id','naziv')
    # for row in rows:
    #     result.append(dict(zip(keys,row)))
    # json_data = json.dumps(result)
    result = list()
    keys = ('Mentor_id','email','Sola_id','naziv')
    row=()
    uci=Uci.objects.filter(Potrjen=1).values()
    for uc in uci:
        user=User.objects.get(id=uc["ID_Mentor_id"])
        sola=Sola.objects.get(ID_Sola=uc["ID_Sola_id"])
        row=(user.id,user.email,uc["ID_Sola_id"],sola.Naziv)
        result.append(dict(zip(keys,row)))

    json_data = json.dumps(result)

    if imeS != "":
        # Ob kliku pri filtriranju ustvari skupino in pogled iz teh mentorjev!
        idMe = [d.get('Mentor_id', None) for d in result]
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        cursor = connection.cursor()

        se = ""
        if len(idMe)>1:
            se = tuple(idMe)
        else:
            se = str(idMe[0])

        my_dict = {
            'gname': imeS,
            'skp': se
        }
        if len(idMe) == 1:
            #SQL za posobljen pogled oz. novo skupino
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(skp)s
                                ''',my_dict)
        else:
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

        # Vstavi uporabnike v dejansko bazo oz. novo skupino
        # id ustvarjene grupe
        group = mail_group.objects.get(name=imeS).idSkupine
        # print(group)
        # print(groupsId)
        for groupid in idMe:
            usId = User.objects.get(id=groupid)
            authg = mail_group.objects.get(idSkupine=group)
            adduser = mail_user_groups(user_id=usId, group_id=authg)
            adduser.save()

    return JsonResponse(json_data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_view_user_competitions(request):
    userId=-1
    try:
        userId = int(request.GET.get("userid"))
    except:
        return Response(status=409)
    if not Sodeluje.objects.filter(ID_Mentor=userId).exists():
        return Response(status=404)
    sez_tek=Sodeluje.objects.filter(ID_Mentor = userId).values("ID_Tekmovanje","ID_Sola")
    for mentor_sola in sez_tek:
        mentor_sola["Naziv"] = Tekmovanje.objects.filter(ID_Tekmovanje = mentor_sola["ID_Tekmovanje"]).values()[0]["Ime_Tekmovanje"]
        mentor_sola["Ime_sola"]=Sola.objects.filter(ID_Sola=mentor_sola["ID_Sola"]).values()[0]["Naziv"]

        data=list(sez_tek)
    return JsonResponse(data, safe=False,status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_user_info_by_id(request,userId):
    try:
        userId = int(request.GET.get("userId"))
    except:
        return Response(status=409)
    usr = User.objects.get(id=userId)
    s = UserSerializer(usr,context={'request': request})
    return Response(s.data)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_competitions_of_type(request,typeId):
    try:
        typeId = int(request.GET.get("typeId"))
    except:
        return Response(status=409)
    idTipa = tip_tekmovanje.objects.get(id=typeId)
    data=list(Tekmovanje.objects.filter(tip_tekmovanja=typeId).values())
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_messages(request):
    cursor = connection.cursor()

    #pridobi vse šole na katerih so nepotrjeni mentorji
    cursor.execute(''' SELECT msg.id,usr.id,usr.email,msg.message,msg.title,msg.created_at FROM tekmovanja_messages as msg, auth_user as usr
    where msg.user_id = usr.id;''')

    rows = cursor.fetchall()

    result = []
    keys = ('id','Mentor_id','email','message','title','created_at')
    for (id,Mentor_id,email,message,title,created_at) in rows:
        result.append(dict(zip(keys,(id,Mentor_id,email,message,title,created_at.strftime("%d/%m/%Y, %H:%M:%S")))))

    json_data = json.dumps(result,indent=4, sort_keys=True, default=str)

    return JsonResponse(json_data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def new_messages(request, last_login):
    try:
        last_login = request.GET.get('last_login')
    except:
        return Response(409)
    return Response(Messages.objects.filter(created_at__gte=last_login).exists())

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_message(request, messageId):
    try:
        messageId= request.GET.get("messageId")
    except:
        return Response("message_id_not_set",status=409)
    Messages.objects.filter(id=messageId).delete()
    return Response(status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def user_school_subscripitons_by_id(request,userid):
    try:
        userId = int(request.GET.get("userid"))
    except:
        return Response(status=409)
    mentorUciNaSolah = Uci.objects.filter(ID_Mentor=userId).values("ID_Sola")
    for sola in mentorUciNaSolah:
        if Uci.objects.filter(ID_Sola=sola["ID_Sola"],ID_Mentor=userId).values("Potrjen")[0]["Potrjen"] == 1:
            sola["Potrjen"]="1"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
        else:
            sola["Potrjen"]="0"
            sola["Naziv"] = Sola.objects.filter(ID_Sola=sola["ID_Sola"]).values("Naziv")[0]["Naziv"]
    data=list(mentorUciNaSolah)
    return JsonResponse(data, safe=False)


@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_change_profile(request):
    try:
        username = request.data['UpIme']
        #email = request.data['email']
        ime = request.data['Ime']
        priimek = request.data['Priimek']
    except:
        return Response("mankajoci_podatki",status=409)

    if "@" in username:
        return Response(status=407)

    current_user = User.objects.filter(id=request.user.id)
    current_user_values = current_user.values().first()

    if len(username) > 25:
        return Response("predolgo_up_ime_ali_email",status=409)

    if current_user_values['username'] != username and User.objects.filter(username=username).exists():
        return Response("up_ime_zasedeno",status=409)

    if current_user_values["username"] == username or len(username) == 0:
        username = current_user_values["username"]

    if current_user_values["first_name"] == ime or len(ime) == 0:
        ime = current_user_values["first_name"]

    if current_user_values["last_name"] == priimek or len(priimek) == 0:
        priimek = current_user_values["last_name"]

    current_user.update(username=username,first_name=ime,last_name=priimek)

    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_change_password(request):
    try:
        geslo = request.data['geslo']
    except:
        return Response(status=409)
    if (len(geslo) < 5):
        return Response(status=409)
    u = User.objects.get(id=request.user.id)
    u.set_password(geslo)
    u.save()
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_change_settings(request):
    try:
        usname = request.data['eposta']
        pas = request.data["gesl"]
    except:
        return Response(status=409)

    #import os
    #import json

    #with open('secrets.json') as json_file:
    #    data = json.load(json_file)

    # Spreminjanje upImena
    #data["HostUser"] = usname
    #data["HostPassword"] = pas

    #with open("secrets.json", 'w') as outfile:
    #    json.dump(data, outfile)

    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_save_settings(request):
    try:
        serName = request.data['serName']
        port = request.data["portN"]
        email = request.data["email"]
        use = request.data["userCreate"]
    except:
        return Response(status=409)

    userId = User.objects.get(id=use)

    if Mail_settings.objects.filter(posta=email).exists():
        return Response(status=405)

    Mail_settings(streznik=serName,st_vrat=port,posta=email,ID_Mentor=userId).save()

    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def set_settings(request):
    try:
        ida = request.data['Id_a']
        idMail = request.data["idmail"]
        geslo = request.data["pass"]
    except:
        return Response(status=409)

    #Pridobi podatke
    pod = list(Mail_settings.objects.filter(ID_Setting = idMail).values())
    strez = pod[0]["streznik"]
    stvrat = pod[0]["st_vrat"]
    posta = pod[0]["posta"]

    # posodobi secrects
    import os
    import json

    # with open('secrets.json') as json_file:
    #     data = json.load(json_file)

    # # Spreminjanje upImena
    # data["EmailHost"] = strez
    # data["EmailPort"] = stvrat
    # data["HostUser"] = posta
    # # data["HostPassword"] = "geslo"


    # with open("secrets.json", 'w') as outfile:
    #     json.dump(data, outfile)

    from django.conf import settings

    settings.EMAIL_HOST=strez
    settings.EMAIL_PORT=stvrat
    settings.EMAIL_HOST_USER=posta
    settings.EMAIL_HOST_PASSWORD=geslo

    return Response(status=201)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_user_settings(request,idU):
    try:
        id = request.GET.get("idU")
    except:
        return Response(status=409)

    userId = User.objects.get(id=id)
    data=list(Mail_settings.objects.filter(ID_Mentor=userId).values())
    return JsonResponse(data, safe=False)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_admin_user(request):
    userId=request.user.id
    try: 
        user=User.objects.get(id=userId)
    except:
        return Response("user not found", 404)

    #superadmin also staff member
    if User.objects.filter(is_staff = 1, is_active = 1).count() == 2:
        return Response("zadnji_admin")
    else:
        User.objects.filter(id=userId).update(is_active=0)

    return Response(status=201)


@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def uncorfirmed_mentors_exists(request):
    try:
        last_login= request.GET.get("last_login")
    except:
        return Response("required parameters not found",status=400)

    return Response(Uci.objects.filter(created_at__gte=last_login,Potrjen=0).exists())

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def add_competition_type(request):
    try:
        competition_type = request.data["competition_type"]
    except:
        return Response("required parameters not found",status=400)
    if tip_tekmovanje.objects.filter(naziv=competition_type).exists():
        return Response("tip tekmovanja že obstaja", status=400)
    tip_tekmovanje(naziv=competition_type).save()
    return Response(status=201)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_competition_types(request):
    data=list(tip_tekmovanje.objects.all().values())
    return JsonResponse(data, safe=False)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_competition_type(request, competitionTypeId):
    try:
        competition_id= request.GET.get("competitionTypeId")
    except:
        return Response("required parameters not found",status=400)
    if Tekmovanje.objects.filter(tip_tekmovanja=competition_id).exists():
        return Response("uporabljeno na tekmovanju", status=400)
    tip_tekmovanje.objects.filter(id=competition_id).delete()
    return Response(status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def users_on_school(request):
    scholls = -1
    save = []
    try:
        scholls=list(request.data["schoolsName"])
        imeS = request.data["imeS"]
    except:
        return Response(status=409)
    for school in scholls:
        trimed = school.strip()
        idSole = Sola.objects.get(Naziv=trimed).ID_Sola
        ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
        save.append(ment)
    if len(save) == 1:
        queryNoDuplicates = save[0]
    else:
        from itertools import chain
        mergeQuerySets = list(chain(*save))
        queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]
    data = list(queryNoDuplicates)

    if imeS != "":
        # Ob kliku pri filtriranju ustvari skupino in pogled iz teh mentorjev!
        idMe = [d.get('ID_Mentor', None) for d in queryNoDuplicates]
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        cursor = connection.cursor()

        se = ""
        if len(idMe)>1:
            se = tuple(idMe)
        else:
            se = str(idMe[0])

        my_dict = {
            'gname': imeS,
            'skp': se
        }
        if len(idMe) == 1:
            #SQL za posobljen pogled oz. novo skupino
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(skp)s
                                ''',my_dict)
        else:
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

        # Vstavi uporabnike v dejansko bazo oz. novo skupino
        # id ustvarjene grupe
        group = mail_group.objects.get(name=imeS).idSkupine
        # print(group)
        # print(groupsId)
        for groupid in idMe:
            usId = User.objects.get(id=groupid)
            authg = mail_group.objects.get(idSkupine=group)
            adduser = mail_user_groups(user_id=usId, group_id=authg)
            adduser.save()
    return JsonResponse(data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def add_mentors_to_group(request):
    try:
        mentors=list(request.data["mentors"])
        imeS = request.data["imeS"]
    except:
        return Response(status=409)

    my_dict1 = {
        'gname': imeS
    }

    # Preveri ali je element ali funkcija
    cursor = connection.cursor()
    fun_const = mail_group.objects.get(name=imeS).fun_or_const
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini

        cursor.execute('''CREATE VIEW ''' + my_dict1["gname"] + ''' AS
                        SELECT "user"."id","user"."username"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(gname)s''',my_dict1)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        
    # Posodobi pogled
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + imeS + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]
    for ino in mentors:
        gid.append(int(ino))

    my_dict = {
        'gname': imeS,
        'skp': tuple(gid)
    }
    print("DEBUG::::")
    print(my_dict["skp"])
    cursor1.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s''',my_dict)

    # Dodaj uporabnika v grupo
    idSkp = mail_group.objects.get(name=imeS)
    for mento in mentors:
        idMentorja = User.objects.get(id=int(mento))
        mail_user_groups(user_id=idMentorja, group_id=idSkp).save()
    return Response(201)


@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_mentors_in_group(request):
    try:
        mentors=list(request.data["mentors"])
        imeS = request.data["imeS"]
    except:
        return Response(status=409)

    my_dict1 = {
        'gname': imeS
    }

    # Preveri ali je element ali funkcija
    cursor = connection.cursor()
    fun_const = mail_group.objects.get(name=imeS).fun_or_const
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini

        cursor.execute('''CREATE VIEW ''' + my_dict1["gname"] + ''' AS
                        SELECT "user"."id","user"."username"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(gname)s''',my_dict1)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)

    # Posodobi pogled
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + imeS + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]

    #print(mentors)
    #print(gid)
    mentors = [int(i) for i in mentors]
    #print(mentors)
    lst3 = [value for value in gid if value not in mentors]
    #print(lst3)
    my_dict = {
        'gname': imeS,
        'skp': tuple(lst3)
    }
    cursor1.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

    # Briši uporabnika iz grupe
    idSkp = mail_group.objects.get(name=imeS)
    for mento in mentors:
        #print(mento)
        idMentorja = User.objects.get(id=mento)
        #mail_user_groups(user_id=idMentorja, group_id=idSkp).delete()
        pola = mail_user_groups.objects.get(user_id=idMentorja, group_id=idSkp)
        pola.delete()
    return Response(201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def users_on_competitions(request):
    competitions = -1
    save = []
    try:
        competitions=list(request.data["competitionsName"])
        imeS = request.data["imeS"]
    except:
        return Response(status=409)
    for competition in competitions:
        trimed = competition.strip()
        idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed).ID_Tekmovanje
        ment = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
        save.append(ment)
    if len(save) == 1:
        queryNoDuplicates = save[0]
    else:
        from itertools import chain
        mergeQuerySets = list(chain(*save))
        queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]
    data = list(queryNoDuplicates)

    if imeS != "":
        # Ob kliku pri filtriranju ustvari skupino in pogled iz teh mentorjev!
        idMe = [d.get('ID_Mentor', None) for d in queryNoDuplicates]
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        cursor = connection.cursor()

        se = ""
        if len(idMe)>1:
            se = tuple(idMe)
        else:
            se = str(idMe[0])

        my_dict = {
            'gname': imeS,
            'skp': se
        }
        if len(idMe) == 1:
            #SQL za posobljen pogled oz. novo skupino
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(skp)s
                                ''',my_dict)
        else:
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

        # Vstavi uporabnike v dejansko bazo oz. novo skupino
        # id ustvarjene grupe
        group = mail_group.objects.get(name=imeS).idSkupine
        # print(group)
        # print(groupsId)
        for groupid in idMe:
            usId = User.objects.get(id=groupid)
            authg = mail_group.objects.get(idSkupine=group)
            adduser = mail_user_groups(user_id=usId, group_id=authg)
            adduser.save()
    return JsonResponse(data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def filter_all_combinations_view(request):
    potrjeni = ""
    schools = -1
    competitions = -1
    name = ""
    save = []
    save2 = []
    queryNoDuplicates = []
    try:
        potrjeni = str(request.data["confirmed"])
        schools=list(request.data["schools"])
        competitions=list(request.data["competitions"])
        name = str(request.data["name"])
    except:
        return Response(status=409)
    # print("Potrjeni: " + potrjeni)
    # print("\n Šole: \n")
    # print(schools)
    # print("\n Tekmovanja: \n")
    # print(competitions)
    # print("\n Ime skupune:  \n")
    # print(name)

    cursor = connection.cursor()
    # Filter po vseh mentorjih
    po = filter_all_combinations2(potrjeni,schools,competitions)
    idM = [d.get('ID_Mentor', None) for d in po]
    #print(idM)

    #Pridobi user iz skupine
    skid = mail_group.objects.get(name=name).idSkupine
    mentsk = list(mail_user_groups.objects.filter(group_id=skid).values())
    mentskid = [e.get('user_id_id', None) for e in mentsk]

    #print(mentskid)

    # Presek obeh tabel
    novaSkp = [value for value in idM if value in mentskid]

    print(novaSkp)
    my_dict = {
            'gname': name,
            'skp': tuple(novaSkp)
    }

    # Preveri ali je element ali funkcija
    fun_const = mail_group.objects.get(name=name).fun_or_const
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini
        cursor.execute('''CREATE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff","user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user.id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(gname)s''',my_dict)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=name).update(fun_or_const=0)

    #SQL za posobljen pogled oz. novo skupino
    cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                      SELECT "user"."id","user"."username"
                      FROM "auth_user" as "user"
                      WHERE "user"."id" IN %(skp)s

                        ''',my_dict)

    #Dejansko posodobi skupino v bazi
    #Pridobi mentorje v novi skupini
    # Napolni skupino s posodobljenim pogledom
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + name + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]

    #pobrisi vse mentorje v skupini
    id = mail_group.objects.get(name=name).idSkupine
    mail_user_groups.objects.filter(group_id=id).delete()

    for groupid in gid:
        usId = User.objects.get(id=groupid)
        authg = mail_group.objects.get(idSkupine=id)
        adduser = mail_user_groups(user_id=usId, group_id=authg).save()

    return Response(201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def filter_all_combinations(request):
    potrjeni = ""
    imeS = ""
    schools = -1
    competitions = -1
    save = []
    save2 = []
    queryNoDuplicates = []
    try:
        potrjeni = str(request.data["confirmed"])
        schools=list(request.data["schools"])
        competitions=list(request.data["competitions"])
        imeS = request.data["imeS"]
    except:
        return Response(status=409)
    # print("Potrjeni: " + potrjeni)
    # print("\n Šole: \n")
    # print(schools)
    # print("\n Tekmovanja: \n")
    # print(competitions)

    # KOMBINACIJE
    # Vsi potrjeni/nepotrjeni mentorji, ki so na določenih šolah ali šoli
    if potrjeni != "not" and schools != [] and competitions == []:
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            #.filter(Potrjen=1).values("ID_Mentor")
            if potrjeni == "potrjeni":
                ment = Uci.objects.filter(ID_Sola=idSole).filter(Potrjen=1).values("ID_Mentor")
            elif potrjeni == "nepotrjeni":
                ment = Uci.objects.filter(ID_Sola=idSole).filter(Potrjen=0).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

    # Vsi potrjeni/nepotrjeni mentorji, ki so prijavljeni na določena tekmovanja ali tekmovanje
    elif potrjeni != "not" and schools == [] and competitions != []:
        for compte in competitions:
            trimed = compte.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed).ID_Tekmovanje
            idment = list(Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor"))
            print(idment)
            # potrjen
            if potrjeni == "potrjeni":
                if len(idment) == 1:
                    aprroved = Uci.objects.filter(ID_Mentor=idment).values("Potrjen")
                    if len(aprroved) == 1:
                        if aprroved[0]["Potrjen"] == 1:
                            save.append(Uci.objects.filter(ID_Mentor=idment).values("ID_Mentor"))
                    else:
                        # če je več zapisov istega mentorja v Uci
                        for pot in list(aprroved):
                            if pot["Potrjen"] == 1:
                                save.append(Uci.objects.filter(ID_Mentor=idment).filter(Potrjen=1).values("ID_Mentor"))
                                break
                else:
                    print("PRIDEM")
                    for nk in idment:
                        print(nk)
                        aprroved = Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("Potrjen")
                        if len(aprroved) == 1:
                            if aprroved[0]["Potrjen"] == 1:
                                save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("ID_Mentor"))
                        else:
                            # če je več zapisov istega mentorja v Uci
                            for pot in list(aprroved):
                                if pot["Potrjen"] == 1:
                                    save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).filter(Potrjen=1).values("ID_Mentor"))
                                    break
            elif potrjeni == "nepotrjeni":
                if len(idment) == 1:
                    aprroved = Uci.objects.filter(ID_Mentor=idment).values("Potrjen")
                    if len(aprroved) == 1:
                        if aprroved[0]["Potrjen"] == 0:
                            save.append(Uci.objects.filter(ID_Mentor=idment).values("ID_Mentor"))
                    else:
                        # če je več zapisov istega mentorja v Uci
                        for pot in list(aprroved):
                            if pot["Potrjen"] == 0:
                                save.append(Uci.objects.filter(ID_Mentor=idment).filter(Potrjen=1).values("ID_Mentor"))
                                break
                else:
                    print("PRIDEM")
                    for nk in idment:
                        print(nk)
                        aprroved = Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("Potrjen")
                        if len(aprroved) == 1:
                            if aprroved[0]["Potrjen"] == 0:
                                save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("ID_Mentor"))
                        else:
                            # če je več zapisov istega mentorja v Uci
                            for pot in list(aprroved):
                                if pot["Potrjen"] == 0:
                                    save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).filter(Potrjen=1).values("ID_Mentor"))
                                    break
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

    # Vsi mentorji, ki so na neki šoli in so prijavljeni na neko tekmovanje in obratno
    elif (potrjeni == "not" or potrjeni == "") and schools != [] and competitions != []:
        #Vsi mentorji prijavljeni na neki šoli/šolah
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

        #Vsi mentorji prijavljeni na izbrano tekmovanje
        for competition in competitions:
            trimed1 = competition.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed1).ID_Tekmovanje
            ment1 = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
            save2.append(ment1)
        if len(save2) == 1:
            queryNoDuplicates1 = save2[0]
        else:
            from itertools import chain
            mergeQuerySets1 = list(chain(*save2))
            queryNoDuplicates1 = [i for n, i in enumerate(mergeQuerySets1) if i not in mergeQuerySets1[n + 1:]]

        #Presek teh dveh množic
        queryNoDuplicates = [value for value in list(queryNoDuplicates) if value in list(queryNoDuplicates1)]

    # Če so izbrane vse skupine na enkrat
    elif potrjeni != "not" and schools != [] and competitions != []:
        potr = 1
        if potrjeni == "nepotrjeni":
            potr = 0
        cursor = connection.cursor()
        cursor.execute('''SELECT "user"."id","user"."email", "sola"."ID_Sola","sola"."Naziv" FROM "auth_user" as "user", "Uci" as "uci", "tekmovanja_sola" as "sola"
                        WHERE "uci"."ID_Mentor" = "user"."id" and
                        "sola"."ID_Sola" = "uci"."ID_Sola" and
                        "user"."is_staff" = false and
                        "uci"."Potrjen" = ''' + str(potr))

        rows = cursor.fetchall()
        result = []

        for row in rows:
            o = dict({"ID_Mentor":row[0]})
            result.append(o)
        #print("RESULT")
        #print(result)

        #Vsi mentorji prijavljeni na neki šoli/šolah
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

        #Vsi mentorji prijavljeni na izbrano tekmovanje
        for competition in competitions:
            trimed1 = competition.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed1).ID_Tekmovanje
            ment1 = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
            save2.append(ment1)
        if len(save2) == 1:
            queryNoDuplicates1 = save2[0]
        else:
            from itertools import chain
            mergeQuerySets1 = list(chain(*save2))
            queryNoDuplicates1 = [i for n, i in enumerate(mergeQuerySets1) if i not in mergeQuerySets1[n + 1:]]

        print(queryNoDuplicates)
        print(queryNoDuplicates1)
        # operacije nad množicami
        potrjenostSole = [value for value in list(result) if value in list(queryNoDuplicates)]
        skupajTekmo = [value for value in list(potrjenostSole) if value in list(queryNoDuplicates1)]

        queryNoDuplicates = skupajTekmo

    data = list(queryNoDuplicates)

    if imeS != "":
        # Ob kliku pri filtriranju ustvari skupino in pogled iz teh mentorjev!
        idMe = [d.get('ID_Mentor', None) for d in data]
        mail_group.objects.filter(name=imeS).update(fun_or_const=0)
        cursor = connection.cursor()

        se = ""
        if len(idMe)>1:
            se = tuple(idMe)
        else:
            se = str(idMe[0])

        my_dict = {
            'gname': imeS,
            'skp': se
        }
        if len(idMe) == 1:
            #SQL za posobljen pogled oz. novo skupino
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(skp)s
                                ''',my_dict)
        else:
            cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(skp)s
                                ''',my_dict)

        # Vstavi uporabnike v dejansko bazo oz. novo skupino
        # id ustvarjene grupe
        group = mail_group.objects.get(name=imeS).idSkupine
        # print(group)
        # print(groupsId)
        for groupid in idMe:
            usId = User.objects.get(id=groupid)
            authg = mail_group.objects.get(idSkupine=group)
            adduser = mail_user_groups(user_id=usId, group_id=authg)
            adduser.save()

    return JsonResponse(data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def admin_send_message(request):
    recipients = []
    subject = ""
    message = ""
    html = ""
    try:
        recipients=list(request.data["recipients"])
        subject = str(request.data["subject"])
        message = str(request.data["message"])
        html = str(request.data["html"])
    except:
        return Response(status=409)
    # print("\n SUBJECT: \n")
    # print(subject)
    # print("\n MESSAGE: \n")
    # print(message)
    from_email = ''
    from django.conf import settings

    # print("HOST USER: " + settings.EMAIL_HOST)
    # print("EMAIL_PORT: " + str(settings.EMAIL_PORT))
    # print("EMAIL_HOST_USER: " + settings.EMAIL_HOST_USER)
    # print("HOST USER: " + settings.EMAIL_HOST)
    # print("EMAIL_HOST_PASSWORD: " + settings.EMAIL_HOST_PASSWORD)

    # TODO: optimizacija
    from django.core.mail import EmailMultiAlternatives
    try:
        import os
        lst = os.listdir("media")
        #print(lst)
        for recipient in recipients:
            msg = EmailMultiAlternatives(subject, message, from_email, [recipient])
            msg.attach_alternative(html, "text/html")
            if len(lst)!= 0:
                if len(lst) == 1:
                    msg.attach_file("media/" + lst[0])
                else:
                    for lste in lst:
                        msg.attach_file("media/" + lste)
            msg.send()
    except:
        for f in lst:
            os.remove("media/" + f)
        return Response(status=405)

    # Po poslanem sporočilu izbriši vsebino media
    for f in lst:
        os.remove("media/" + f)

    settings.EMAIL_HOST_PASSWORD = ""
    return Response(200)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def change_group_name(request):
    groupName = ""
    placeH = ""
    try:
        groupName = str(request.data["groupsName"])
        placeH = str(request.data["placeH"])
    except:
        return Response(status=409)
    if mail_group.objects.filter(name=groupName).exists():
        return Response(status=405)


    # Posodobi ime pogleda
    # Pridobi pod. iz prejšnjega pogleda

    cursor1 = connection.cursor()

    #Če pogled obstaja če je skupina fun. ali element!
    fun_const = mail_group.objects.get(name=placeH).fun_or_const

    if fun_const == 0:
        cursor1 = connection.cursor()
        cursor1.execute(''' SELECT id FROM ''' + placeH + '''''')
        rows = cursor1.fetchall()
        gid = [list(i)[0] for i in rows]

        se = ""
        if len(gid)>1:
            se = tuple(gid)
        else:
            se = str(gid[0])

        my_di = {
            'nam': groupName,
            'old': placeH,
            'gi' : se
        }

        if len(gid) == 1:
            cursor1.execute('''CREATE VIEW ''' + my_di["nam"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" = %(gi)s

                                ''',my_di)
        else:
            cursor1.execute('''CREATE VIEW ''' + my_di["nam"] + ''' AS
                            SELECT "user"."id","user"."username"
                            FROM "auth_user" as "user"
                            WHERE "user"."id" IN %(gi)s

                                ''',my_di)

        #Izbriši pogled
        cursor1.execute(''' DROP VIEW IF EXISTS ''' + my_di["old"] + '''''')

        mail_group.objects.filter(name=placeH).update(name=groupName)
    else:
        mail_group.objects.filter(name=placeH).update(name=groupName)

    return Response(201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def delete_group(request):
    gName = ""
    try:
        gName=str(request.data["group_name"])
    except:
        return Response(status=409)
    if not mail_group.objects.filter(name=gName).exists():
        return Response(status=404)

    #Če pogled obstaja če je skupina fun. ali element!
    fun_const = mail_group.objects.get(name=gName).fun_or_const
    if fun_const == 0:
        #Izbriši pogled
        cursor1 = connection.cursor()
        cursor1.execute(''' DROP VIEW IF EXISTS ''' + gName + '''''')

    idGroup = mail_group.objects.get(name=gName).idSkupine
    mail_user_groups.objects.filter(group_id=idGroup).delete()
    mail_group.objects.filter(idSkupine = idGroup).delete()

    return Response(status=202)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_elementary_schools(request):
    data=list(Sola.objects.filter(Vrsta_Sola=0).values())
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def get_high_schools(request):
    data=list(Sola.objects.filter(Vrsta_Sola=1).values())
    return JsonResponse(data, safe=False)

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsStaffUser))
def if_global(request,name):
    try:
        name = str(request.GET.get("name"))
    except:
        return Response(status=409)
    data = list(mail_group.objects.filter(name=name).values())
    return JsonResponse(data, safe=False)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def set_global(request):
    try:
        name = str(request.data["name"])
        gl = str(request.data["globa"])
    except:
        return Response(status=409)
    mail_group.objects.filter(name=name).update(glob=gl)
    return Response(201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def cmentors_view(request):
    try:
        name = str(request.data["name"])
        state = str(request.data["state"])
    except:
        return Response(status=409)

    cursor = connection.cursor()
    my_dict = {
            'gname': name,
            'iff': name,
            'state':state
    }
    # Preveri ali je element ali funkcija
    fun_const = mail_group.objects.get(name=name).fun_or_const
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini

        cursor.execute('''CREATE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(iff)s''',my_dict)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=name).update(fun_or_const=0)

    # Ustvarjen pogled za skupino name
    # Posodobi pogled na potrjene/nepotrjene in popravi v bazi da je funkcija
    cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                    SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                    FROM "auth_user" as "user", "Uci" as "uci", "tekmovanja_sola" as "sola", "mail_group" as "grupa", "mail_user_groups" as "ug"
                    WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(iff)s
                    and "uci"."ID_Mentor" = "user"."id" and
                    "sola"."ID_Sola" = "uci"."ID_Sola" and
                    "user"."is_staff" = false and
                    "uci"."Potrjen" = %(state)s''',my_dict)

    #Dejansko posodobi skupino v bazi
    #Pridobi mentorje v novi skupini
    # Napolni skupino s posodobljenim pogledom
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + name + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]

    #pobrisi vse mentorje v skupini
    id = mail_group.objects.get(name=name).idSkupine
    mail_user_groups.objects.filter(group_id=id).delete()

    for groupid in gid:
        usId = User.objects.get(id=groupid)
        authg = mail_group.objects.get(idSkupine=id)
        adduser = mail_user_groups(user_id=usId, group_id=authg).save()
    return Response(201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def usr_on_school_view(request):
    try:
        schools=list(request.data["school"])
        namea = str(request.data["n"])
    except:
        return Response(status=409)
    #print(schools)
    # ['Oš Krmelj1', 'Oš Mirna']
    se = ""
    if len(schools)>1:
        se = tuple(schools)
    else:
        se = str(schools[0])

    my_dict = {
            'gname': namea,
            'pog': se
    }
    cursor = connection.cursor()

    # Preveri ali je element ali funkcija
    fun_const = mail_group.objects.get(name=namea).fun_or_const
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini
        cursor.execute('''CREATE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(gname)s''',my_dict)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=namea).update(fun_or_const=0)

    #Filter
    if len(schools) == 1:
        cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id", "user"."username"
                            FROM "auth_user" as "user","mail_group" as "grupa", "mail_user_groups" as "ug", "tekmovanja_sola" as "sola","uci" as "uc"
                            WHERE "ug"."user_id" = "user"."id"
                            and "ug"."group_id" = "grupa"."idSkupine"
                            and "grupa"."name"= %(gname)s
                            and "uc"."ID_Mentor"="user"."id"
                            and "uc"."ID_Sola" = "sola"."ID_Sola"
                            and "sola"."Naziv" = %(pog)s

                            ''',my_dict)
    else:
        cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                            SELECT "user"."id", "user"."username"
                            FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug", "tekmovanja_sola" as "sola","uci" as "uc"
                            WHERE "ug"."user_id" = "user"."id"
                            and "ug"."group_id" = "grupa"."idSkupine"
                            and "grupa"."name"= %(gname)s
                            and "uc"."ID_Mentor"="user"."id"
                            and "uc"."ID_Sola" = "sola"."ID_Sola"
                            and "sola"."Naziv" IN %(pog)s GROUP BY "user"."id"

                            ''',my_dict)

    cursor.execute(''' ALTER TABLE tekmovanja_sola CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    #ALTER TABLE my_table_name CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

    #Dejansko posodobi skupino v bazi
    #Pridobi mentorje v novi skupini
    # Napolni skupino s posodobljenim pogledom
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + namea + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]

    #pobrisi vse mentorje v skupini
    id = mail_group.objects.get(name=namea).idSkupine
    mail_user_groups.objects.filter(group_id=id).delete()

    #Napolni tabelo
    for groupid in gid:
        usId = User.objects.get(id=groupid)
        authg = mail_group.objects.get(idSkupine=id)
        adduser = mail_user_groups(user_id=usId, group_id=authg).save()

    return Response(201)


@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def usr_on_competition_view(request):
    try:
        competit=list(request.data["competit"])
        namea = str(request.data["nam"])
    except:
        return Response(status=409)

    se = ""
    if len(competit)>1:
        se = tuple(competit)
    else:
        se = str(competit[0])

    my_dict = {
            'gname': namea,
            'pog': se
    }
    cursor = connection.cursor()

    # Preveri ali je element ali funkcija
    fun_const = mail_group.objects.get(name=namea).fun_or_const
    print(fun_const)
    if fun_const == 1:
        # POMENI DA JE ELEMENT
        # najprej ustvari pogled iz mentorjev v tej skupini
        cursor.execute('''CREATE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug"
                        WHERE "ug"."user_id" = "user"."id" and "ug"."group_id" = "grupa"."idSkupine" and "grupa"."name"= %(gname)s''',my_dict)
        # daj v bazo da je funkcija
        mail_group.objects.filter(name=namea).update(fun_or_const=0)

    #print(my_dict)

    if len(competit) == 1:
        cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug", "tekmovanje" as "tekmo", "sodeluje" as "sode"
                        WHERE "ug"."user_id" = "user"."id"
                        and "ug"."group_id" = "grupa"."idSkupine"
                        and "grupa"."name"= %(gname)s
                        and "sode"."ID_Mentor" = "user"."id"
                        and "sode"."ID_Tekmovanje" = "tekmo"."ID_Tekmmovanje"
                        and "tekmo"."Ime_Tekmovanje" = %(pog)s
                                ''',my_dict)
    else:
        cursor.execute('''CREATE OR REPLACE VIEW ''' + my_dict["gname"] + ''' AS
                        SELECT "user"."id","user"."last_login","user"."is_superuser","user"."username","user"."first_name","user"."last_name","user"."email","user"."is_staff", "user"."is_active","user"."date_joined"
                        FROM "auth_user" as "user", "mail_group" as "grupa", "mail_user_groups" as "ug", "tekmovanje" as "tekmo", "sodeluje" as "sode"
                        WHERE "ug"."user_id" = "user"."id"
                        and "ug"."group_id" = "grupa"."idSkupine"
                        and "grupa"."name"= %(gname)s
                        and "sode"."ID_Mentor" = "user"."id"
                        and "sode"."ID_Tekmovanje" = "tekmo"."ID_Tekmmovanje"
                        and "tekmo"."Ime_Tekmovanje" IN %(pog)s GROUP BY "user"."id"
                                ''',my_dict)

    cursor.execute('''ALTER TABLE tekmovanje CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    cursor.execute('''ALTER TABLE auth_user CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    cursor.execute('''ALTER TABLE mail_group CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    cursor.execute('''ALTER TABLE mail_user_groups CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    cursor.execute('''ALTER TABLE sodeluje CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci; ''')
    #ALTER TABLE my_table_name CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

    #Dejansko posodobi skupino v bazi
    #Pridobi mentorje v novi skupini
    # Napolni skupino s posodobljenim pogledom
    cursor1 = connection.cursor()
    cursor1.execute(''' SELECT id FROM ''' + namea + '''''')
    rows = cursor1.fetchall()
    gid = [list(i)[0] for i in rows]

    # #pobrisi vse mentorje v skupini
    id = mail_group.objects.get(name=namea).idSkupine
    mail_user_groups.objects.filter(group_id=id).delete()

    # #Napolni tabelo
    for groupid in gid:
        usId = User.objects.get(id=groupid)
        authg = mail_group.objects.get(idSkupine=id)
        adduser = mail_user_groups(user_id=usId, group_id=authg).save()
    return Response(201)


def filter_all_combinations2(potrjeni,schools,competitions):
    save = []
    save2 = []
    queryNoDuplicates = []

    # KOMBINACIJE
    # Vsi potrjeni/nepotrjeni mentorji, ki so na določenih šolah ali šoli
    if potrjeni != "not" and schools != [] and competitions == []:
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            #.filter(Potrjen=1).values("ID_Mentor")
            if potrjeni == "potrjeni":
                ment = Uci.objects.filter(ID_Sola=idSole).filter(Potrjen=1).values("ID_Mentor")
            elif potrjeni == "nepotrjeni":
                ment = Uci.objects.filter(ID_Sola=idSole).filter(Potrjen=0).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

    # Vsi potrjeni/nepotrjeni mentorji, ki so prijavljeni na določena tekmovanja ali tekmovanje
    elif potrjeni != "not" and schools == [] and competitions != []:
        for compte in competitions:
            trimed = compte.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed).ID_Tekmovanje
            idment = list(Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor"))
            #print(idment)
            # potrjen
            if potrjeni == "potrjeni":
                if len(idment) == 1:
                    aprroved = Uci.objects.filter(ID_Mentor=idment).values("Potrjen")
                    if len(aprroved) == 1:
                        if aprroved[0]["Potrjen"] == 1:
                            save.append(Uci.objects.filter(ID_Mentor=idment).values("ID_Mentor"))
                    else:
                        # če je več zapisov istega mentorja v Uci
                        for pot in list(aprroved):
                            if pot["Potrjen"] == 1:
                                save.append(Uci.objects.filter(ID_Mentor=idment).filter(Potrjen=1).values("ID_Mentor"))
                                break
                else:
                    #print("PRIDEM")
                    for nk in idment:
                        #print(nk)
                        aprroved = Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("Potrjen")
                        if len(aprroved) == 1:
                            if aprroved[0]["Potrjen"] == 1:
                                save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("ID_Mentor"))
                        else:
                            # če je več zapisov istega mentorja v Uci
                            for pot in list(aprroved):
                                if pot["Potrjen"] == 1:
                                    save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).filter(Potrjen=1).values("ID_Mentor"))
                                    break
            elif potrjeni == "nepotrjeni":
                if len(idment) == 1:
                    aprroved = Uci.objects.filter(ID_Mentor=idment).values("Potrjen")
                    if len(aprroved) == 1:
                        if aprroved[0]["Potrjen"] == 0:
                            save.append(Uci.objects.filter(ID_Mentor=idment).values("ID_Mentor"))
                    else:
                        # če je več zapisov istega mentorja v Uci
                        for pot in list(aprroved):
                            if pot["Potrjen"] == 0:
                                save.append(Uci.objects.filter(ID_Mentor=idment).filter(Potrjen=1).values("ID_Mentor"))
                                break
                else:
                    #print("PRIDEM")
                    for nk in idment:
                        #print(nk)
                        aprroved = Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("Potrjen")
                        if len(aprroved) == 1:
                            if aprroved[0]["Potrjen"] == 0:
                                save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).values("ID_Mentor"))
                        else:
                            # če je več zapisov istega mentorja v Uci
                            for pot in list(aprroved):
                                if pot["Potrjen"] == 0:
                                    save.append(Uci.objects.filter(ID_Mentor=nk["ID_Mentor"]).filter(Potrjen=1).values("ID_Mentor"))
                                    break
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

    # Vsi mentorji, ki so na neki šoli in so prijavljeni na neko tekmovanje in obratno
    elif (potrjeni == "not" or potrjeni == "") and schools != [] and competitions != []:
        #Vsi mentorji prijavljeni na neki šoli/šolah
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

        #Vsi mentorji prijavljeni na izbrano tekmovanje
        for competition in competitions:
            trimed1 = competition.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed1).ID_Tekmovanje
            ment1 = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
            save2.append(ment1)
        if len(save2) == 1:
            queryNoDuplicates1 = save2[0]
        else:
            from itertools import chain
            mergeQuerySets1 = list(chain(*save2))
            queryNoDuplicates1 = [i for n, i in enumerate(mergeQuerySets1) if i not in mergeQuerySets1[n + 1:]]

        #Presek teh dveh množic
        queryNoDuplicates = [value for value in list(queryNoDuplicates) if value in list(queryNoDuplicates1)]

    # Če so izbrane vse skupine na enkrat
    elif potrjeni != "not" and schools != [] and competitions != []:
        potr = 1
        if potrjeni == "nepotrjeni":
            potr = 0
        cursor = connection.cursor()
        cursor.execute('''SELECT "user"."id","user"."email", "sola"."ID_Sola","sola"."Naziv" FROM "auth_user" as "user", "Uci" as "uci", "tekmovanja_sola" as "sola"
                        WHERE "uci"."ID_Mentor" = "user"."id" and
                        "sola"."ID_Sola" = "uci"."ID_Sola" and
                        "user"."is_staff" = false and
                        "uci"."Potrjen" = ''' + str(potr))

        rows = cursor.fetchall()
        result = []

        for row in rows:
            o = dict({"ID_Mentor":row[0]})
            result.append(o)
        #print("RESULT")
       # print(result)

        #Vsi mentorji prijavljeni na neki šoli/šolah
        for school in schools:
            trimed = school.strip()
            idSole = Sola.objects.get(Naziv=trimed).ID_Sola
            ment = Uci.objects.filter(ID_Sola=idSole).values("ID_Mentor")
            save.append(ment)
        if len(save) == 1:
            queryNoDuplicates = save[0]
        else:
            from itertools import chain
            mergeQuerySets = list(chain(*save))
            queryNoDuplicates = [i for n, i in enumerate(mergeQuerySets) if i not in mergeQuerySets[n + 1:]]

        #Vsi mentorji prijavljeni na izbrano tekmovanje
        for competition in competitions:
            trimed1 = competition.strip()
            idTekmovanja = Tekmovanje.objects.get(Ime_Tekmovanje=trimed1).ID_Tekmovanje
            ment1 = Sodeluje.objects.filter(ID_Tekmovanje=idTekmovanja).values("ID_Mentor")
            save2.append(ment1)
        if len(save2) == 1:
            queryNoDuplicates1 = save2[0]
        else:
            from itertools import chain
            mergeQuerySets1 = list(chain(*save2))
            queryNoDuplicates1 = [i for n, i in enumerate(mergeQuerySets1) if i not in mergeQuerySets1[n + 1:]]

        #print(queryNoDuplicates)
        #print(queryNoDuplicates1)
        # operacije nad množicami
        potrjenostSole = [value for value in list(result) if value in list(queryNoDuplicates)]
        skupajTekmo = [value for value in list(potrjenostSole) if value in list(queryNoDuplicates1)]

        queryNoDuplicates = skupajTekmo
    return list(queryNoDuplicates)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsStaffUser))
def save_file(request):
    # Izbriši prej morebitne datoteke
    import os
    lst = os.listdir("media")
    for f in lst:
        os.remove("media/" + f)

    fe = request.FILES['file']
    # TODO: Shrani fe objekt datoteke
    #print("SM TUKJ NOTR")

    dolzina = len(request.FILES.getlist("file"))
    sez = request.FILES.getlist("file")
    fs = FileSystemStorage()
    if dolzina == 1:
        filename = fs.save(fe.name, fe)
    else:
        for i in range(len(sez)):
            filename = fs.save(sez[i].name, sez[i])

    return Response(201)