from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.shortcuts import redirect
from rest_framework import generics
from django.contrib import auth
from rest_framework.permissions import BasePermission
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from tekmovanja.models import user_email_change
from tekmovanja.models import User
from tekmovanja.api.guest import deleteUserIfNotVerifiedEmail


#here are all templates for user
def checkUserPermissions(user):
    return user.is_authenticated and not user.is_superuser and not user.is_staff

class user_index(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)
    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({'homeActive':"Active"}, template_name='user/index.html')
        else:
            return redirect('login')

class user_profile(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({"userActive":"Active","profileActive":"Active"}, template_name='user/user_profile/user_profile.html')
        else:
            return redirect('login')
class send_message_to_admin(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({"userActive":"Active","send_message_to_admin":"Active"}, template_name='user/message/send_message_to_admin.html')
        else:
            return redirect('login')


class schools(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({'schoolActive':"Active"}, template_name='user/school/schools.html')
        else:
            return redirect('login')

class competitions(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({'competitionActive':"Active"}, template_name='user/competitions/competitions.html')
        else:
            return redirect('login')

class help(generics.RetrieveAPIView):

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkUserPermissions(request.user):
            return Response({"userActive":"Active","helpActive":"Active"}, template_name='user/help.html')
        else:
            return redirect('login')

class change_email(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, token):
        try:
            uec = user_email_change.objects.get(token=token)
        except:
            return Response(template_name="user/change_email_unsuccess.html")
     
        user_id = uec.user_id.id
        new_email = uec.new_email

        #check if in mean time is some user already registrired and confirmed account
        if User.objects.filter(email=new_email, is_active=1).exists():
            #delete email from temporary table
            uec.delete()
            return Response(template_name="user/change_email_unsuccess.html")
        
        #delete email from temporary table
        uec.delete()

        #check if in mean time is some user already registrired and not confirmed account
        if User.objects.filter(email=new_email, is_active=0).exists():
            if(deleteUserIfNotVerifiedEmail(new_email,3600) == True):
                User.objects.filter(id=user_id).update(email=new_email)
            else:
                return Response(template_name="user/change_email_unsuccess.html")
        else:
            User.objects.filter(id=user_id).update(email=new_email)
        return Response(template_name="user/change_email_success.html")

class web_logout(generics.RetrieveAPIView):

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if request.user.is_authenticated:
            auth.logout(request)
        return redirect('login')

