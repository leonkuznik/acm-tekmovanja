from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import *
admin.site.site_header='Administrator'
admin.site.register(Verifikacija)
admin.site.register(Pozadbljenogesloverifikacija)
admin.site.register(Sola)
admin.site.register(Potrditve)
admin.site.register(Uci)
admin.site.register(Tekmovanje)
admin.site.register(Sodeluje)
admin.site.register(email_merge)
admin.site.register(Skritatekmovanja)
admin.site.register(user_help)
admin.site.register(Messages)
admin.site.register(Mail_settings)
admin.site.register(mail_group)
admin.site.register(mail_user_groups)

