from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from tekmovanja import views
from tekmovanja import api
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.http import HttpResponseRedirect
import django.views.static
from django.views.static import serve
from django.conf import settings
from django.urls import re_path

# APIJI NAMENJENI SUPERUSERJU
router = routers.SimpleRouter()
router.register(r'users', api.UserViewSet)
router.register(r'groups', api.GroupViewSet)
router.register(r'sola', api.SolaViewSet)
router.register(r'mentor', api.MentorViewSet)
router.register(r'potrditve', api.PotrditveViewSet)
router.register(r'uci', api.UciViewSet)
router.register(r'tekmovanje', api.TekmovanjeViewSet)
router.register(r'sodeluje', api.SodelujeViewSet)
router.register(r'mail_group', api.Mail_groupViewSet)
router.register(r'mail_user_groups', api.Mail_user_groupsViewSet)
router.register(r'mail_settings', api.Mail_settingsViewSet)

urlpatterns = [

    #...........................................................
    # VSI URL-JI ZA APLIKACIJO
    #...........................................................

    #...........................................................
    # TEMPLATE
    #...........................................................

    #...........................................................
    # SERVIRANJE HTML DATOTEK app/tekmovanja/templates/*
    #...........................................................

    #JAVNI HTML-JI (app/tekmovanja/views/view_guest.py, app/tekmovanja/templates/guest/*)
    #----------------------------
    url(r'^registration/$', views.registration_view.as_view(), name='registration'),
    url(r'^pogoji_uporabe/$', views.pogoji_uporabe.as_view(), name='pogoji_uporabe'),
    url(r'^$', views.login_view.as_view(), name='login'),
    url(r'^login/$', lambda r: HttpResponseRedirect('/')),
    url(r'^lost_password/$', views.lost_password.as_view(), name='lost_password'),
    url(r'^lost_password_verify/(?P<token>\w{0,70})/$',views.lost_password_verify.as_view(),name='lost_password_verify'),
    url(r'^merge_users_request/(?P<token>\w{0,70})/$', views.merge_users_request.as_view(),name='merge_users_request'),
    url(r'^all_schools/$', views.all_schools.as_view(), name='all_schools'),

    #HTML-JI ZA NAVADNEGA UPORABNIKA (app/tekmovanja/views/view_user.py, app/tekmovanja/templates/user/*)
    #----------------------------
    url(r'^user_index/$', views.user_index.as_view(), name='user_index'),
    url(r'^schools/$', views.schools.as_view(), name='schools'),
    url(r'^competitions/$', views.competitions.as_view(), name='competitions'),
    url(r'^user_profile/$', views.user_profile.as_view(),name='user_profile'),
    url(r'^confirmed/\w{0,5}/$', views.confirmed.as_view(),name='confirmed'),
    url(r'^unconfirmed/\w{0,5}/$', views.unconfirmed.as_view(),name='uncorfirmed'),
    url(r'^send_message_to_admin/$', views.send_message_to_admin.as_view(),name='send_message_to_admin'),
    url(r'^help/$', views.help.as_view(),name='help'),
    url(r'^change_email/(?P<token>\w{0,70})/$', views.change_email.as_view(),name='change_email'),

    #HTML-JI ZA UPRAVITELJA TEKMOVANJ (app/tekmovanja/views/view_admin.py, app/tekmovanja/templates/admin_user/*)
    #----------------------------
    url(r'^admin_index/$', views.admin_index.as_view(), name='admin_index'),
    url(r'^admin_add_schools/$', views.admin_add_schools.as_view(), name='admin_add_schools'),
    url(r'^admin_bulk_schools/$', views.admin_bulk_schools.as_view(), name='admin_bulk_schools'),
    url(r'^admin_add_competitions/$', views.admin_add_competitions.as_view(), name='admin_add_competitions'),
    url(r'^admin_add_competition_type/$', views.admin_add_competition_type.as_view(), name='admin_add_competition_type'),
    url(r'^admin_view_competition_type/$', views.admin_view_competition_type.as_view(), name='admin_view_competition_type'),
    url(r'^admin_add_admin/$', views.admin_add_admin.as_view(), name='admin_add_admin'),
    url(r'^admin_send/$', views.admin_send.as_view(), name='admin_send'),
    url(r'^admin_view/users/$', views.admin_view_users.as_view(), name='admin_view_users'),
    url(r'^admin_view_competitions/$', views.admin_view_competitions.as_view(), name='admin_view_competitions'),
    url(r'^admin_view_uncorfirmed_menthors/$', views.uncorfirmed_menthors.as_view(), name='uncorfirmed_menthors'),
    url(r'^admin_view_schools/$', views.admin_view_schools.as_view(), name='admin_view_schools'),
    url(r'^admin_view_help/$', views.admin_view_help.as_view(), name='admin_view_help'),
    url(r'^admin_edit_profile/$', views.admin_edit_profile.as_view(), name='admin_edit_profile'),
    url(r'^verifikacija/(?P<token>\w{0,70})/$', views.verifikacija.as_view()),
    url(r'^documentation/$', views.documentation.as_view(), name='documentation'),
    url(r'^admin_view_first_unconfirmed/$', views.first_unconfirmed.as_view(), name='first_unconfirmed'),
    url(r'^admin_add_help/$', views.admin_add_help.as_view(), name='admin_add_help'),
    url(r'^messages/$', views.messages.as_view(), name='messages'),
    url(r'^web_logout/$', views.web_logout.as_view(),name='web_logout'),

    #HTML-JI ZA SUPERADMINA (app/tekmovanja/views/view_superuser.py, app/tekmovanja/templates/superuser/*)
    #----------------------------
    url(r'^admin/', admin.site.urls),
    url(r'^admin/bulk/$', views.admin_bulk.as_view()),
    url(r'^admin/merge/$', views.admin_merge.as_view()),


    #...........................................................
    # API
    #...........................................................

    #...........................................................
    # SERVIRANJE API-JEV NAMENJENIH APLIKACIJI  (tekmovanja/api)
    #...........................................................

    #JAVNI API-JI 
    #----------------------------
    url(r'^api/guest/login_phone/$', api.create_login_phone),
    url(r'^api/guest/registration_call/$', api.registration),
    url(r'^api/guest/get_seznam_naziv_sol_call/$',api.get_seznam_naziv_sol),
    url(r'^api/get_public_schools/$', api.get_public_schools),
    url(r'^api/guest/send_lost_password_email/$',api.send_lost_password_email),
    url(r'^api/guest/send_lost_password_token/$',api.send_lost_password_token),

    #API-JI NAMENJENI NAVADNEMU UPORABNIKU 
    #----------------------------
    url(r'^api/user/get_user_and_change_last_login/$', api.get_user_and_change_last_login),
    url(r'^api/user/competition_login/$', api.competition_login),
    url(r'^api/user/sodeluje_tekmovanje/$', api.sodeluje_tekmovanje),
    url(r'^api/user/competition_info/$', api.competition_info),
    url(r'^api/user/get_user_info/$', api.get_user_info),
    url(r'^api/user/school_users/(?P<schoolId>\w{0,4})$', api.school_users),
    url(r'^api/user/user_competition_subscripitons/(?P<competitionId>\w{0,4})$', api.user_competition_subscripitons),
    url(r'^api/user/school_unregistration/$', api.school_unregistration),
    url(r'^api/user/delete_user/$', api.delete_user),
    url(r'^api/user/user_competitions/$', api.user_competitions),
    url(r'^api/user/user_add_schools/$', api.user_add_schools),
    url(r'^api/user/user_change_profile/$', api.user_change_profile),
    url(r'^api/user/user_change_password/$', api.user_change_password),
    url(r'^api/user/user_school_subscripitons/$', api.user_school_subscripitons),
    url(r'^api/user/new_competitions/(?P<last_login>\w{0,30})$',api.new_competitions),
    url(r'^api/user/user_has_unconfirmed_mentors/(?P<last_login>\w{0,30})$',api.user_has_unconfirmed_mentors),
    url(r'^api/user/hide_competition/$',api.hide_competition),
    url(r'^api/user/un_hide_competition/$',api.un_hide_competition),
    url(r'^api/user/send_message/$',api.send_message),

    #API-JI NAMENJENI UPRAVITELJU TEKMOVANJ (ADMINU) 
    #----------------------------
    #GET
    url(r'^api/upravitelj_tekmovanj/get_user_by_id/(?P<userId>\w{0,5})$', api.get_user_info_by_id),
    url(r'^api/upravitelj_tekmovanj/get_competitions_of_type/(?P<typeId>\w{0,5})$', api.get_competitions_of_type),
    url(r'^api/upravitelj_tekmovanj/user_school_subscripitons_by_id/(?P<userid>\w{0,5})$', api.user_school_subscripitons_by_id),
    url(r'^api/upravitelj_tekmovanj/get_menthors_by_competition/(?P<competitionId>\w{0,5})$', api.get_menthors_by_competition),
    url(r'^api/upravitelj_tekmovanj/list_users/$', api.list_users),
    url(r'^api/upravitelj_tekmovanj/get_schools/$', api.get_schools),
    url(r'^api/upravitelj_tekmovanj/get_competitions/$', api.get_competitions),
    url(r'^api/upravitelj_tekmovanj/get_competitions_types/$', api.get_competitions_types),
    url(r'^api/upravitelj_tekmovanj/get_uncorfirmed_menthors/$', api.get_uncorfirmed_menthors),
    url(r'^api/upravitelj_tekmovanj/get_uncorfirmed_menthors_by_school/$', api.get_uncorfirmed_menthors_by_school),
    url(r'^api/upravitelj_tekmovanj/get_competition/$', api.get_competition),
    url(r'^api/upravitelj_tekmovanj/get_school/$', api.get_school),
    url(r'^api/upravitelj_tekmovanj/admin_view_user_competitions/$', api.admin_view_user_competitions),
    url(r'^api/upravitelj_tekmovanj/get_first_uncorfirmed_menthor/$', api.get_first_uncorfirmed_menthor),
    url(r'^api/upravitelj_tekmovanj/get_first_uncorfirmed_exists/$', api.get_first_uncorfirmed_exists),
    url(r'^api/upravitelj_tekmovanj/get_messages/$', api.get_messages),
    url(r'^api/upravitelj_tekmovanj/new_messages/(?P<last_login>\w{0,30})$', api.new_messages),

    #POST
    url(r'^api/upravitelj_tekmovanj/change_competition/$', api.change_competition),
    url(r'^api/upravitelj_tekmovanj/change_school_name/$', api.change_school_name),
    url(r'^api/upravitelj_tekmovanj/add_admin_user/$', api.add_admin_user),
    url(r'^api/upravitelj_tekmovanj/admin_add_school_call/$', api.admin_add_school_call),
    url(r'^api/upravitelj_tekmovanj/bulk_school/$', api.bulk_school),
    url(r'^api/upravitelj_tekmovanj/admin_add_competition_call/$', api.admin_add_competition_call),
    url(r'^api/upravitelj_tekmovanj/admin_change_profile/$', api.admin_change_profile),
    url(r'^api/upravitelj_tekmovanj/admin_change_password/$', api.admin_change_password),
    url(r'^api/upravitelj_tekmovanj/uncorfirmed_mentors_exists/$',api.uncorfirmed_mentors_exists),
    url(r'^api/upravitelj_tekmovanj/add_competition_type/$',api.add_competition_type),
    url(r'^api/upravitelj_tekmovanj/get_competition_types/$',api.get_competition_types),
    url(r'^api/upravitelj_tekmovanj/add_help/$',api.add_help),

    #DELETE
    url(r'^api/upravitelj_tekmovanj/delete_competition/(?P<competitionId>\w{0,5})$', api.delete_competition),
    url(r'^api/upravitelj_tekmovanj/delete_school/(?P<schoolId>\w{0,5})$', api.delete_school),
    url(r'^api/upravitelj_tekmovanj/delete_competition_type/(?P<competitionTypeId>\w{0,5})$', api.delete_competition_type),
    url(r'^api/upravitelj_tekmovanj/delete_admin_user/$', api.delete_admin_user),
    url(r'^api/upravitelj_tekmovanj/delete_help/(?P<helpId>\w{0,5})$',api.delete_help),
    url(r'^api/upravitelj_tekmovanj/delete_message/(?P<messageId>\w{0,5})$',api.delete_message),


    #API-JI NAMENJENI UPRAVITELJU TEKMOVANJ IN NAVADNEMU UPORABNIKU
    #----------------------------
    url(r'^api/shared/confirm_user/$', api.confirm_user),
    url(r'^api/shared/get_help/$', api.get_help),

    #API-JI NAMENJENI SUPERADMINU 
    #----------------------------
    path('', include(router.urls)),
    url(r'^api/superuser/bulk_users/$', api.bulk_users),
    url(r'^api/superuser/merge_users/$', api.merge_users),

    #...........................................................
    # DEL APLIKACIJE ZA OBVEŠČANJE MENTORJEV IN TVORJENJA SKUPIN
    #...........................................................

    #POŠILJANJE E-POŠTE
    #----------------------------
    url(r'^api/upravitelj_tekmovanj/get_user_settings/(?P<idU>\w{0,5})$', api.get_user_settings),

    #POST
    url(r'^api/upravitelj_tekmovanj/admin_save_settings/$', api.admin_save_settings),
    url(r'^api/upravitelj_tekmovanj/set_settings/$', api.set_settings),
    url(r'^api/upravitelj_tekmovanj/admin_change_settings/$', api.admin_change_settings),
    url(r'^api/upravitelj_tekmovanj/admin_send_message/$', api.admin_send_message),
    url(r'^api/upravitelj_tekmovanj/save_file/$', api.save_file),

    #TVORJENJE, UREJANJE IN FILTRIRANJE SKUPIN TER POSODABLJANJE POGLEDOV
    #-----------------------------------------------------------------------
    url(r'^api/upravitelj_tekmovanj/if_global/(?P<name>\w{0,30})$', api.if_global),
    url(r'^api/upravitelj_tekmovanj/get_groups/(?P<idA>\w{0,5})$', api.get_groups),
    url(r'^api/upravitelj_tekmovanj/get_users_from_groups/(?P<group_name>\w{0,5})$', api.get_users_from_groups),
    url(r'^api/upravitelj_tekmovanj/get_users_from_groups_withData/(?P<group_name>\w{0,5})$', api.get_users_from_groups_withData),
    url(r'^api/upravitelj_tekmovanj/get_users_from_groups_all/(?P<group_name>\w{0,5})$', api.get_users_from_groups_all),
    url(r'^api/upravitelj_tekmovanj/get_elementary_schools/$', api.get_elementary_schools),
    url(r'^api/upravitelj_tekmovanj/get_high_schools/$', api.get_high_schools),

    #POST
    url(r'^api/upravitelj_tekmovanj/set_global/$', api.set_global),
    url(r'^api/upravitelj_tekmovanj/add_mentors_to_group/$', api.add_mentors_to_group),
    url(r'^api/upravitelj_tekmovanj/delete_mentors_in_group/$', api.delete_mentors_in_group),
    url(r'^api/upravitelj_tekmovanj/users_on_school/$', api.users_on_school),
    url(r'^api/upravitelj_tekmovanj/users_on_competitions/$', api.users_on_competitions),
    url(r'^api/upravitelj_tekmovanj/filter_all_combinations/$', api.filter_all_combinations),
    url(r'^api/upravitelj_tekmovanj/users_in_groups/$', api.users_in_groups),
    url(r'^api/upravitelj_tekmovanj/admin_add_group_call/$', api.admin_add_group_call),
    url(r'^api/upravitelj_tekmovanj/change_group_name/$', api.change_group_name),
    url(r'^api/upravitelj_tekmovanj/delete_group/$', api.delete_group),
    url(r'^api/upravitelj_tekmovanj/get_corfirmed_menthors/$', api.get_corfirmed_menthors),
    url(r'^api/upravitelj_tekmovanj/get_uncorfirmed_menthors2/$', api.get_uncorfirmed_menthors2),

    #USTVARJANJE POGLEDOV
    #---------------------

    #POST
    url(r'^api/upravitelj_tekmovanj/cmentors_view/$', api.cmentors_view),
    url(r'^api/upravitelj_tekmovanj/usr_on_school_view/$', api.usr_on_school_view),
    url(r'^api/upravitelj_tekmovanj/usr_on_competition_view/$', api.usr_on_competition_view),
    url(r'^api/upravitelj_tekmovanj/filter_all_combinations_view/$', api.filter_all_combinations_view),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)